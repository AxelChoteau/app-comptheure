package com.afpa.comptheure.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.afpa.comptheure.objet.CDI;

public class MySQLiteHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "bdd.db";
    private static final int DATABASE_VERSION = 2;

    public static SQLiteDatabase stmt;

    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        stmt = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(FormationTable.CREATE_TABLE_CREATE);
//        stmt = this.getWritableDatabase();
        CDI.ajout=true;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String strSQL = "drop table "+DATABASE_NAME;
        db.execSQL(strSQL);
        this.onCreate(db);
    }
}