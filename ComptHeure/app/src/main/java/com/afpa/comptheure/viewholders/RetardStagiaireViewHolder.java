package com.afpa.comptheure.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.afpa.comptheure.R;
import com.afpa.comptheure.adapter.RetardStagiaireAdapter;

import lombok.Getter;

public class RetardStagiaireViewHolder extends RecyclerView.ViewHolder{
    @Getter
    private TextView stagiaireName;
    @Getter
    private TextView stagiaireRetard;

    private RetardStagiaireAdapter adapter;

    public RetardStagiaireViewHolder(View v, RetardStagiaireAdapter adapter){
        super(v);
        this.adapter=adapter;
        stagiaireName=itemView.findViewById(R.id.retard_stagiaire_name);
        stagiaireRetard=itemView.findViewById(R.id.retard_stagiaire);

    }
}
