package com.afpa.comptheure.tools;

import com.afpa.comptheure.objet.Stagiaire;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Retard {

    public static String getRetard(String email) throws ParseException {
        int nbAbs = 0;
        int nbRetard = 0;
        int nbRetardMin = 0;
        int nbRetardH = 0;
        String retard = "";
        SimpleDateFormat sdf = new SimpleDateFormat("MM/yyyy");
        Date date = new Date();

        ArrayList<String> list = Stagiaire.selectLate(email);
        ArrayList<String> list2 = new ArrayList<String>();

        // Boucle pour la lecture de la list
        for (String a : list) {
            String[] parts = a.split("; ");
            // sdf.parse(a.substring(a.indexOf("/") + 1, a.indexOf(";")).trim());
            a = a.substring(a.indexOf('/') + 1, a.indexOf(';'));
            if (a.equals(sdf.format(date))) {// !list2.contains(a)
                list2.add(a);
            }
            // Condition pour la lecture du code
            if (parts[2].equals("10")) {
                nbAbs++;
                // Condition pour le retard
            } else {
                nbRetard += Integer.parseInt(parts[3].trim());
                nbRetardMin = nbRetard % 60;
                nbRetardH = nbRetard / 60;

            }
        }
        return retard = nbAbs + ";" + nbRetardH + "h" + nbRetardMin;

    }

    private static boolean isAcquired(String str) {
        return true;
    }

    private static ArrayList<String> getList() {
        ArrayList<String> list = new ArrayList<String>();

        list.add("01/04/2019; AM; 01; 15");
        list.add("01/03/2019; AM; 01; 15");
        list.add("01/03/2019; PM; 01; 0");
        list.add("02/03/2019; AM; 10; 0");
        list.add("02/03/2019; PM; 01; 0");
        list.add("03/03/2019; AM; 01; 15");
        list.add("03/03/2019; PM; 01; 0");
        list.add("04/03/2019; AM; 01; 0");
        list.add("04/03/2019; PM; 01; 0");
        list.add("05/03/2019; AM; 01; 15");
        list.add("05/03/2019; PM; 01; 0");
        list.add("06/03/2019; AM; 10; 0");
        list.add("06/03/2019; PM; 01; 15");

        return list;
    }

    private static void mise() {

    }
}

