package com.afpa.comptheure.objet;

import android.util.Log;

import static com.afpa.comptheure.constante.ConstanteDataBase.ASCENDANT;
import static com.afpa.comptheure.constante.ConstanteDataBase.ORDER_BY;
import static com.afpa.comptheure.database.StagiaireCompetenceTable.COMPETENCES_STAGIAIRE_COLUMN_ACQUISITION;
import static com.afpa.comptheure.database.StagiaireCompetenceTable.COMPETENCES_STAGIAIRE_COLUMN_COMPETENCES;
import static com.afpa.comptheure.database.StagiaireCompetenceTable.COMPETENCE_STAGIAIRE_COLUMN_CCP;
import static com.afpa.comptheure.constante.ConstanteDataBase.PARENTHESE_FERM_POINT_VIRGULE;
import static com.afpa.comptheure.constante.ConstanteDataBase.ESPACE_PARENTHESE_OUVR;
import static com.afpa.comptheure.database.StagiaireRetardTable.TEMPS_COLUMN_JOURNEE;
import static com.afpa.comptheure.database.StagiaireTable.STAGIAIRE_COLUMN_FIRSTNAME;
import static com.afpa.comptheure.database.StagiaireRetardTable.TEMPS_COLUMN_RETARD;
import static com.afpa.comptheure.constante.ConstanteDataBase.PARENTHESE_OUVR_QUOTE;
import static com.afpa.comptheure.constante.ConstanteDataBase.QUOTE_PARENTHESE_FERM;
import static com.afpa.comptheure.database.StagiaireRetardTable.TEMPS_COLUMN_CODE;
import static com.afpa.comptheure.database.StagiaireRetardTable.TEMPS_COLUMN_DATE;
import static com.afpa.comptheure.constante.ConstanteDataBase.QUOTE_VIRGULE_QUOTE;
import static com.afpa.comptheure.database.StagiaireTable.STAGIAIRE_COLUMN_MAIL;
import static com.afpa.comptheure.database.StagiaireTable.STAGIAIRE_COLUMN_NAME;
import static com.afpa.comptheure.constante.ConstanteDataBase.TEXT_PRIMARY_KEY;
import static com.afpa.comptheure.constante.ConstanteDataBase.PARENTHESE_FERM;
import static com.afpa.comptheure.constante.ConstanteDataBase.PARENTHESE_OUVR;
import static com.afpa.comptheure.constante.ConstanteDataBase.TEXT_NOT_NULL;
import static com.afpa.comptheure.constante.ConstanteDataBase.INT_NOT_NULL;
import static com.afpa.comptheure.constante.ConstanteDataBase.CREATE_TABLE;
import static com.afpa.comptheure.constante.ConstanteDataBase.INSERT_INTO;
import static com.afpa.comptheure.constante.ConstanteDataBase.PRIMARY_KEY;
import static com.afpa.comptheure.constante.ConstanteDataBase.UNDERSCORE;
import static com.afpa.comptheure.constante.ConstanteDataBase.STAGIAIRE;
import static com.afpa.comptheure.constante.ConstanteDataBase.SELECTALL;
import static com.afpa.comptheure.constante.ConstanteDataBase.VIRGULE;
import static com.afpa.comptheure.constante.ConstanteDataBase.SELECT;
import static com.afpa.comptheure.constante.ConstanteDataBase.UPDATE;
import static com.afpa.comptheure.constante.ConstanteDataBase.VALUES;
import static com.afpa.comptheure.constante.ConstanteDataBase.WHERE;
import static com.afpa.comptheure.constante.ConstanteDataBase.SKILL;
import static com.afpa.comptheure.constante.ConstanteDataBase.QUOTE;
import static com.afpa.comptheure.constante.ConstanteDataBase.EGALE;
import static com.afpa.comptheure.constante.ConstanteDataBase.FROM;
import static com.afpa.comptheure.constante.ConstanteDataBase.LATE;
import static com.afpa.comptheure.constante.ConstanteDataBase.AND;
import static com.afpa.comptheure.constante.ConstanteDataBase.SET;
import com.afpa.comptheure.constante.ConstanteDataBase;
import com.afpa.comptheure.database.DatabaseAndroid;
import com.afpa.comptheure.database.StagiaireTable;
import com.afpa.comptheure.tools.Mail;

import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;

import lombok.Data;

/**
 * Classe permettant la gestion des stagiaires
 *
 * @author Thierry
 */
@Data
public class Stagiaire {

    private String formation;
    private String session;
    private String name;
    private String firstname;
    private String email;
    private boolean isSelected;
    private ArrayList<StagiaireCCP> listCcp;

    /**
     * Constructeur avec trois paramètres
     *
     * @param name est le nom du stagiaire
     * @param firstname est le prenom du stagiaire
     * @param email est l'email du stagiaire
     */
    public Stagiaire(String name, String firstname, String email){
        this.name = name;
        this.firstname = firstname;
        this.email = email;
        this.listCcp = new ArrayList<>();
    }

    /**
     * Méthode permettant la creation d'un stagiaire dans la base de données
     */
    public static void createStagiaire(String formation, String session){
        DatabaseAndroid.onCreate(CREATE_TABLE + formation + UNDERSCORE
                + session + UNDERSCORE + STAGIAIRE + ESPACE_PARENTHESE_OUVR + STAGIAIRE_COLUMN_NAME
                + TEXT_NOT_NULL + VIRGULE + STAGIAIRE_COLUMN_FIRSTNAME + TEXT_NOT_NULL + VIRGULE
                + STAGIAIRE_COLUMN_MAIL + TEXT_PRIMARY_KEY + PARENTHESE_FERM_POINT_VIRGULE);
    }

    /**
     * Méthode permettant le choix d'un stagiaire dans la base de données
     */
    public static ArrayList<String> selectStagiaire(String nomSession){
        return DatabaseAndroid.onSelect(SELECTALL + nomSession + ORDER_BY
                + STAGIAIRE_COLUMN_NAME + VIRGULE + STAGIAIRE_COLUMN_FIRSTNAME
                + ASCENDANT);
    }

    /**
     * Méthode permettant l'ajout d'un stagiaire dans la base de données
     */
    public static boolean insertStagiaire(String nameTable, String name, String firstname, String email){
        if (!DatabaseAndroid.onExist(SELECTALL + nameTable + WHERE + STAGIAIRE_COLUMN_MAIL
                + EGALE + QUOTE + email + QUOTE)) {
            DatabaseAndroid.onInsert(INSERT_INTO + nameTable + VALUES + PARENTHESE_OUVR_QUOTE + name
                    + QUOTE_VIRGULE_QUOTE + firstname + QUOTE_VIRGULE_QUOTE + email + QUOTE_PARENTHESE_FERM);
            return true;
        } return false;
    }

    /**
     * Méthode permettant la création de la présence d'un stagiaire dans sa base de données
     */
    public static void createLate(String email){
        DatabaseAndroid.onCreate(CREATE_TABLE + email + UNDERSCORE + LATE
                + ESPACE_PARENTHESE_OUVR + TEMPS_COLUMN_DATE + TEXT_NOT_NULL + VIRGULE
                + TEMPS_COLUMN_JOURNEE + TEXT_NOT_NULL + VIRGULE + TEMPS_COLUMN_CODE
                + TEXT_NOT_NULL + VIRGULE + TEMPS_COLUMN_RETARD + INT_NOT_NULL + VIRGULE
                + PRIMARY_KEY + PARENTHESE_OUVR + TEMPS_COLUMN_DATE + VIRGULE
                + TEMPS_COLUMN_JOURNEE + PARENTHESE_FERM + PARENTHESE_FERM_POINT_VIRGULE);
    }

    /**
     * Méthode permettant la sélection de la présence d'un stagiaire dans sa base de données
     */
    public static ArrayList<String> selectLate (String email){
        return DatabaseAndroid.onSelect(SELECTALL + email + UNDERSCORE + LATE);
    }

    /**
     * Méthode permettant la sélection de la présence d'un stagiaire dans sa base de données
     */
    public static ArrayList<String> selectLateLate (String email, String date, String journee){
        return DatabaseAndroid.onSelect(SELECT + TEMPS_COLUMN_RETARD + FROM + email
                + UNDERSCORE + LATE + WHERE + TEMPS_COLUMN_DATE + EGALE + QUOTE + date
                + QUOTE + AND + TEMPS_COLUMN_JOURNEE + EGALE + QUOTE + journee + QUOTE);
    }

    /**
     * Méthode permettant l'ajout de la présence d'un stagiaire dans sa base de données
     */
    public static boolean insertLate(String email, String date, String journee, String code, int retard) {
        Log.i("Axel", "insertInto Late avant");
        if (!DatabaseAndroid.onExist(SELECT + TEMPS_COLUMN_RETARD + FROM + email + UNDERSCORE
                + LATE + WHERE + TEMPS_COLUMN_DATE + EGALE + QUOTE + date + QUOTE + AND
                + TEMPS_COLUMN_JOURNEE + EGALE + QUOTE + journee + QUOTE)) {
            Log.i("Axel", "insertInto Late in");
            DatabaseAndroid.onInsert(INSERT_INTO + email + UNDERSCORE + LATE + VALUES
                    + PARENTHESE_OUVR_QUOTE + date + QUOTE_VIRGULE_QUOTE + journee + QUOTE_VIRGULE_QUOTE
                    + code + QUOTE + VIRGULE + retard + PARENTHESE_FERM);
            return true;
        }return false;
    }

    /**
     * Méthode pour la mise a jour de retard pour un stagiaire dans sa base de données
     */
    public static boolean updateLate(String email, String date, String journee, int retard){
        if (DatabaseAndroid.onExist(SELECT + TEMPS_COLUMN_CODE + VIRGULE + TEMPS_COLUMN_RETARD
                + FROM + email + UNDERSCORE + LATE + WHERE + TEMPS_COLUMN_DATE + EGALE + QUOTE
                + date + QUOTE + AND + TEMPS_COLUMN_JOURNEE + EGALE + QUOTE + journee + QUOTE)) {
            DatabaseAndroid.onUpdate(UPDATE + email + UNDERSCORE + LATE + SET + TEMPS_COLUMN_CODE
                    + EGALE + QUOTE + "01" + QUOTE +VIRGULE+ TEMPS_COLUMN_RETARD + EGALE + retard + WHERE
                    + TEMPS_COLUMN_DATE + EGALE + QUOTE + date + QUOTE + AND + TEMPS_COLUMN_JOURNEE
                    + EGALE + QUOTE + journee + QUOTE);
            return true;
        }return false;
    }
    /**
     * Méthode permettant la création de compétence pour un stagiare dans sa base de données
     */
    public static void createSkill(String email){
        DatabaseAndroid.onCreate(CREATE_TABLE + email + UNDERSCORE + SKILL
                + ESPACE_PARENTHESE_OUVR + COMPETENCE_STAGIAIRE_COLUMN_CCP
                + TEXT_NOT_NULL + VIRGULE + COMPETENCES_STAGIAIRE_COLUMN_COMPETENCES
                + TEXT_NOT_NULL + VIRGULE + COMPETENCES_STAGIAIRE_COLUMN_ACQUISITION
                + TEXT_NOT_NULL + VIRGULE + PRIMARY_KEY + PARENTHESE_OUVR
                + COMPETENCE_STAGIAIRE_COLUMN_CCP + VIRGULE
                + COMPETENCES_STAGIAIRE_COLUMN_COMPETENCES + PARENTHESE_FERM
                + PARENTHESE_FERM_POINT_VIRGULE);
    }

    /**
     * Méthode permettant l'ajout de compétence pour un stagiare dans sa base de données
     */
    public static boolean insertSkill(String email, String ccp, String competence, String acquisition){
        if (!DatabaseAndroid.onExist( SELECTALL + email + UNDERSCORE + SKILL + WHERE
             + COMPETENCE_STAGIAIRE_COLUMN_CCP + EGALE + QUOTE + ccp + QUOTE + AND
                + COMPETENCES_STAGIAIRE_COLUMN_COMPETENCES + EGALE + QUOTE
                + competence + QUOTE)) {
            DatabaseAndroid.onInsert(INSERT_INTO + email + UNDERSCORE + SKILL + VALUES
                    + PARENTHESE_OUVR_QUOTE + ccp + QUOTE_VIRGULE_QUOTE + competence
                    + QUOTE_VIRGULE_QUOTE + acquisition + QUOTE_PARENTHESE_FERM);
            return true;
        }return false;
    }

    /**
     * Méthode pour la mise a jour de compétence pour un stagiaire dans sa base de données
     */
    public static boolean updateSkill(String email, String ccp, String competence, String acquisition ){
        if (DatabaseAndroid.onExist( SELECTALL + email + UNDERSCORE + SKILL + WHERE
                + COMPETENCE_STAGIAIRE_COLUMN_CCP + EGALE + QUOTE + ccp + QUOTE + AND
                + COMPETENCES_STAGIAIRE_COLUMN_COMPETENCES + EGALE + QUOTE
                + competence + QUOTE)) {
            DatabaseAndroid.onUpdate(UPDATE + email + UNDERSCORE + SKILL + SET
                    + COMPETENCES_STAGIAIRE_COLUMN_ACQUISITION + EGALE + QUOTE
                    + acquisition + QUOTE + WHERE + COMPETENCE_STAGIAIRE_COLUMN_CCP
                    + EGALE + QUOTE + ccp + QUOTE + AND + COMPETENCES_STAGIAIRE_COLUMN_COMPETENCES
                    + EGALE + QUOTE + competence + QUOTE );
            return true;
        }return false;
    }

    public static ArrayList<String> selectSkill(String email){
        return DatabaseAndroid.onSelect(SELECTALL + email + UNDERSCORE
            + SKILL);
    }
    /**
     * Méthode pour la création d'une liste de stagiaire
     *
     * @param stagiaires contient une liste de string
     * @return une list de stagiaire
     */
    public static List <Stagiaire> createListStagiaires(List<String> stagiaires){
        List<Stagiaire> listStagiaires = new ArrayList<>();
        String[] stud;
        for (int i=0; i<stagiaires.size();i++){
            stud=stagiaires.get(i).split(";");
            listStagiaires.add(new Stagiaire(stud[0].trim(), stud[1].trim(), stud[2].trim()));
        }return listStagiaires;
    }

    /**
     * Méthode qui permet l'insertion des compétences dans la liste de compétence du stagiaire
     *
     * @param competences
     * @param email
     * @return
     */
    public static ArrayList <String> createListCompetences (ArrayList <String> competences, String email){

        ArrayList<String> result = new ArrayList<>();
//        if (result.size() != 0){
            for (String a : competences){
                String [] tab = a.split(";");
                insertSkill(email, tab[0].trim(), tab[1].trim(), "n");
            }
//        }
        return result;
    }

    /**
     * Méthode qui permet la création d'objet StagiaireCCp et StagiaireCompetence
     *
     * @param competences contient la liste des compétences en String
     * @return contient la list des compétence sous forme d'objet
     */
    public static List<StagiaireCCP> createObject(ArrayList <String> competences){
        List<StagiaireCCP> listCcp=new ArrayList<>();
        String ccp = "";
        StagiaireCCP ccpObject = null;
        for (int i = 0; i < competences.size(); i++){
            String [] tab = competences.get(i).split(";");
            if (i == 0 || !ccp.equalsIgnoreCase(tab[0])) {
                ccp = tab[0];
                ccpObject = new StagiaireCCP(ccp);
                listCcp.add(ccpObject);
                ccpObject.addCompetence(new StagiaireCompetence(tab[1].trim(), tab[2].trim()));
            }else {
                ccpObject.addCompetence(new StagiaireCompetence(tab[1].trim(), tab[2].trim()));
            }
        }return listCcp;
    }

    /**
     * Méthode qui permet la mise a jour des données en fonction d'un ArrayList
     *
     * @param list contient la liste de compétence a mettre à jour
     * @param email contient l'email de la base de données a mettre a jour
     */
    public static void updateCompetences (List<StagiaireCCP> list, String email){
        for (StagiaireCCP a: list){
            for (StagiaireCompetence b : a.getCompetences()) {
                updateSkill(email, a.getName().trim(), b.getName().trim(), b.getEtat().trim());
            }
        }
    }

    /**
     * Méthode permettant l'ajout des retards
     */
    public static boolean insertIntoList(List<Stagiaire> list, String date, String journee){
        String code = "";
        boolean present = false;
        if (list.size()>0) {
            for (Stagiaire a : list) {
                if (a.isSelected) {
                    code = "01";
                } else {
                    code = "10";
//                    try {
//                        Mail.envoiMail(a);
//                    } catch (MessagingException e) {
//                        e.printStackTrace();
//                    }
                }
                selectLate(a.getEmail().trim().replace("@", "arobase").replace(".", "point"));
                present = insertLate(a.getEmail().trim().replace("@", "arobase").replace(".", "point")
                        , date, journee, code, 0);
            }
        }
        return present;
    }

    public static ArrayList <String> createListLate(List<Stagiaire> list, String date, String journee){

        ArrayList <String> late = new ArrayList<>();

        for (Stagiaire a : list){
            ArrayList<String> stagiaireLate = selectLateLate(a.getEmail().replace("@", "arobase")
                            .replace(".", "point").trim(), date, journee);
            if (stagiaireLate.size() != 0) {
                if (!stagiaireLate.get(0).trim().equals("0")) {
                    late.add(a.name + " " + a.firstname + ";" + stagiaireLate.get(0));
                    Log.i("thierry", a.name + ";" + a.firstname + ";" + stagiaireLate.get(0));
                }
            }
        }
        return late;
    }
}