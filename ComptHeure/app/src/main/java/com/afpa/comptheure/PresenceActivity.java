package com.afpa.comptheure;

import com.afpa.comptheure.adapter.PresenceStagiaireAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import com.afpa.comptheure.objet.Stagiaire;
import android.support.v4.app.Fragment;
import android.widget.CompoundButton;
import android.view.LayoutInflater;
import android.widget.CheckBox;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;
import android.view.View;
import android.os.Bundle;

public class PresenceActivity extends Fragment {

    private RecyclerView listStagiaire;
    private CheckBox selectAll;
    private Button applyChanges;

    private GestionActivity activity;

    public static PresenceActivity newInstance(GestionActivity activity){
        PresenceActivity presenceFrag = new PresenceActivity();
        presenceFrag.activity=activity;
        return presenceFrag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.presence_content, container, false);

        listStagiaire = view.findViewById(R.id.presence_stagiaire_list);
        listStagiaire.setHasFixedSize(true);
        listStagiaire.setLayoutManager(new LinearLayoutManager(activity));
        final PresenceStagiaireAdapter adapter = new PresenceStagiaireAdapter(activity);
        listStagiaire.setAdapter(adapter);
        selectAll = view.findViewById(R.id.presence_select_all);
        selectAll.setChecked(adapter.isAllSelected());
        selectAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                adapter.setAllSelected(isChecked);
                adapter.notifyDataSetChanged();
            }
        });
        applyChanges = view.findViewById(R.id.presence_apply);
        applyChanges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Stagiaire.insertIntoList(activity.getListStagiaires(), activity.getTxtDate().getText().toString().replaceAll("/","").trim(),activity.getASwitch().getText().toString().trim())){
                    Toast.makeText(activity.getApplicationContext(), "Les presences ont déjà été validées pour cette date", Toast.LENGTH_LONG).show();
                }

            }
        });
        return view;
    }
}