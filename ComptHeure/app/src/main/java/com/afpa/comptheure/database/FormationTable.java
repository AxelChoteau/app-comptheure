package com.afpa.comptheure.database;

import android.content.Context;
import com.afpa.comptheure.constante.ConstanteDataBase;

/**
 * Classe gérant la table de formation
 *
 * @author Cassandra
 */
public class FormationTable {

    public static final String FORMATION_TABLE_NAME = "formation";
    public static final String FORMATION_COLUMN_NAME = "nom";
    public static final String CREATE_TABLE_CREATE = ConstanteDataBase.CREATE_TABLE + FORMATION_TABLE_NAME
                    + ConstanteDataBase.ESPACE_PARENTHESE_OUVR + FORMATION_COLUMN_NAME + ConstanteDataBase.TEXT_PRIMARY_KEY
                    + ConstanteDataBase.PARENTHESE_FERM_POINT_VIRGULE;
    public static final String FORMATION_SELECT_ALL = ConstanteDataBase.SELECTALL + FORMATION_TABLE_NAME
            + ConstanteDataBase.ORDER_BY + FORMATION_COLUMN_NAME;

    /**
     * Constructeur avec un paramètre
     *
     * @param context contient le context de la page
     */
    public FormationTable(Context context) {
        new MySQLiteHelper(context);
    }
}