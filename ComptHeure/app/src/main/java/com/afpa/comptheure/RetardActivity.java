package com.afpa.comptheure;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.afpa.comptheure.adapter.RetardStagiaireAdapter;
import com.afpa.comptheure.popup.PopupRetard;
import com.afpa.comptheure.tools.Retard;

import lombok.Getter;

public class RetardActivity extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";

    private int mPage;

    private RecyclerView listRetard;

    private Button applyChanges;

    private GestionActivity activity;
@Getter
    private RetardStagiaireAdapter adapter;

    public static RetardActivity newInstance(GestionActivity activity){
        RetardActivity retardFrag = new RetardActivity();
        retardFrag.activity=activity;
        return retardFrag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //mPage = getArguments().getInt(ARG_PAGE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.retard_content, container, false);
        listRetard=view.findViewById(R.id.retard_stagiaire_list);
        listRetard.setHasFixedSize(true);
        listRetard.setLayoutManager(new LinearLayoutManager(activity));

        adapter = new RetardStagiaireAdapter(activity, activity.getRetards());
        listRetard.setAdapter(adapter);

        applyChanges=view.findViewById(R.id.retard_apply);
        applyChanges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new PopupRetard(activity, activity.getString(R.string.retard_title), R.layout.popup_retard, adapter).initPopup();
            }
        });
        return view;
    }

    public void applyChanges(){
        adapter.notifyDataSetChanged();
    }
}
