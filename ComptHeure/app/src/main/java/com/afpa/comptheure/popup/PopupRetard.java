package com.afpa.comptheure.popup;

import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import com.afpa.comptheure.GestionActivity;
import com.afpa.comptheure.R;
import com.afpa.comptheure.adapter.RetardStagiaireAdapter;
import com.afpa.comptheure.adapter.SpinnerAdapterRetard;
import com.afpa.comptheure.adapter.SpinnerAdapterStagiaire;
import com.afpa.comptheure.objet.Stagiaire;

import java.util.ArrayList;
import java.util.List;

public class PopupRetard extends Popup {

    private RetardStagiaireAdapter adapter;
    private Spinner stagiaireList;
    private Spinner retardList;
    private Button newRetardButton;
    private Button newRetardCancelButton;
    public PopupRetard(AppCompatActivity activity, String title, int layout, RetardStagiaireAdapter adapter){
        super(activity,title,layout);
        this.adapter=adapter;
    }
    public static List<String> listRetard= new ArrayList<>();

    static{
        listRetard.add("0");
        listRetard.add("15");
        listRetard.add("30");
        listRetard.add("45");
        listRetard.add("60");
        listRetard.add("75");
        listRetard.add("90");
        listRetard.add("105");
        listRetard.add("120");
        listRetard.add("135");
        listRetard.add("150");
        listRetard.add("165");
        listRetard.add("180");
    }

    @Override
    protected void initPopupViewControls() {
        // Get layout inflater object.
        LayoutInflater layoutInflater = LayoutInflater.from(activity);

        // Inflate the popup dialog from a layout xml file.
        popupView = layoutInflater.inflate(layout, null);
        final GestionActivity gestionActivity = (GestionActivity) activity;
        // Get user input edittext and button ui controls in the popup dialog.
        stagiaireList = popupView.findViewById(R.id.new_retard_stagiaire);
        SpinnerAdapterStagiaire stagiaireAdapter = new SpinnerAdapterStagiaire(activity, R.layout.retard_spinner, gestionActivity.getListStagiaires());
        stagiaireList.setAdapter(stagiaireAdapter);

        retardList=popupView.findViewById(R.id.new_retard_retard);
        SpinnerAdapterRetard retardAdapter = new SpinnerAdapterRetard(activity, R.layout.retard_spinner, listRetard);
        retardList.setAdapter(retardAdapter);

        newRetardButton = popupView.findViewById(R.id.new_retard_button);
        newRetardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Stagiaire stagiaire = (Stagiaire) stagiaireList.getSelectedItem();
                Stagiaire.updateLate(stagiaire.getEmail().replace("@", "arobase").replace(".", "point").trim()
                    , gestionActivity.getTxtDate().getText().toString().replaceAll("/", "").trim()
                    ,gestionActivity.getASwitch().getText().toString().trim(), Integer.parseInt(retardList.getSelectedItem().toString()));
                adapter.addRetard(stagiaire.getName()+" "+stagiaire.getFirstname()+";"+retardList.getSelectedItem().toString());
                adapter.notifyDataSetChanged();
                alertDialog.cancel();
            }
        });

        newRetardCancelButton = popupView.findViewById(R.id.new_retard_cancel_button);
        newRetardCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
    }
}