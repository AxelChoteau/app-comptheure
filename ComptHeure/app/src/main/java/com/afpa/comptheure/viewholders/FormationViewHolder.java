package com.afpa.comptheure.viewholders;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.afpa.comptheure.CompetenceActivity;
import com.afpa.comptheure.MainActivity;
import com.afpa.comptheure.R;
import com.afpa.comptheure.adapter.FormationAdapter;
import com.afpa.comptheure.adapter.SessionAdapter;
import com.afpa.comptheure.listener.TypeFormationListener;
import com.afpa.comptheure.objet.Sessions;
import com.afpa.comptheure.popup.PopupSession;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FormationViewHolder extends RecyclerView.ViewHolder {
    private View v;
    private TextView formationName;
    private LinearLayout subItem;
    private RecyclerView sessions;
    private FloatingActionButton newSession;
    private Button modifyCompetence;
    private ImageView imgExpand;
    private MainActivity activity;

    private String formation;
    private FormationAdapter adapter;
    public FormationViewHolder(View v, FormationAdapter adapter, final MainActivity activity){
        super(v);
        this.v=v;
        this.activity=activity;
        final FormationAdapter adapt=adapter;
        formationName= itemView.findViewById(R.id.main_holder_formation);
        subItem= itemView.findViewById(R.id.main_holder_subitem);
        sessions=itemView.findViewById(R.id.main_holder_sessions);
        imgExpand=itemView.findViewById(R.id.main_expand);
        newSession=itemView.findViewById(R.id.main_holder_new_session);
        modifyCompetence=itemView.findViewById(R.id.main_holder_modify_competence);
        modifyCompetence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(activity, CompetenceActivity.class);
                intent.putExtra("formation", formationName.getText().toString());
                activity.startActivity(intent);
            }
        });
        newSession.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                new PopupSession(activity, activity.getString(R.string.session_title), R.layout.main_popup_newsession, adapt , formation).initPopup();
            }
        });
        itemView.setOnClickListener(new TypeFormationListener(this.adapter));
    }

    public void bind(String formation){
        formationName.setText(formation);
        sessions.setHasFixedSize(true);
        this.formation = formation.trim();
        sessions.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
        SessionAdapter sAdapter = new SessionAdapter(Sessions.selectSession(formation.trim()), activity);
        sessions.setAdapter(sAdapter);
    }
}