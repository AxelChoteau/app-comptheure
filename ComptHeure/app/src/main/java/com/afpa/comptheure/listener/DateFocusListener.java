package com.afpa.comptheure.listener;

import android.app.DatePickerDialog;
import android.content.Context;
import android.text.InputType;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import java.util.Calendar;

public class DateFocusListener implements View.OnFocusChangeListener {

    private Context context;
    private int mYear, mMonth, mDay;

    public DateFocusListener(Context context){
        this.context=context;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        final EditText txtDate = (EditText) v;
        if (hasFocus) {
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            String date= ((monthOfYear + 1)<10?"0":"")+(monthOfYear + 1) + "/" + year;
                            txtDate.setText(date);

                        }
                    }, mYear, mMonth, mDay);

            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
            txtDate.setInputType(InputType.TYPE_NULL);
            if(txtDate.getText() != null){
                txtDate.clearFocus();
            }
        }
    }
}
