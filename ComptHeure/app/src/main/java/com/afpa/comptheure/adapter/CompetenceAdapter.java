package com.afpa.comptheure.adapter;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afpa.comptheure.R;
import com.afpa.comptheure.viewholders.CompetenceViewHolder;
import com.afpa.comptheure.viewholders.SessionViewHolder;

import java.util.List;

public class CompetenceAdapter extends RecyclerView.Adapter<CompetenceViewHolder> {

    private AppCompatActivity activity;
    private List<String> competenceList;

    public CompetenceAdapter(AppCompatActivity activity, List<String> competenceList){
        this.activity=activity;
        this.competenceList=competenceList;

    }
    @NonNull
    @Override
    public CompetenceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.competence_competence_holder, parent, false);
        return new CompetenceViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CompetenceViewHolder viewHolder, int position) {
        viewHolder.getTxtCompetenceName().setText(competenceList.get(position));
    }

    @Override
    public int getItemCount() {
        return competenceList.size();
    }
}
