package com.afpa.comptheure.database;

import static com.afpa.comptheure.constante.ConstanteDataBase.SELECTALL;
import static com.afpa.comptheure.constante.ConstanteDataBase.ESPACE;
import static com.afpa.comptheure.constante.ConstanteDataBase.EGALE;
import static com.afpa.comptheure.constante.ConstanteDataBase.WHERE;
import static com.afpa.comptheure.constante.ConstanteDataBase.QUOTE;
import android.database.sqlite.SQLiteDatabase;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import java.util.ArrayList;
import java.util.List;

public class StagiaireCompetenceTable  {
    private MySQLiteHelper dbHelper;
    private SQLiteDatabase stmt;

    public static  String COMPETENCE_STAGIAIRE_TABLE_NAME ;
    public static final String COMPETENCE_STAGIAIRE_COLUMN_CCP = "ccp";
    public static final String COMPETENCES_STAGIAIRE_COLUMN_COMPETENCES = "competences";
    public static final String COMPETENCES_STAGIAIRE_COLUMN_ACQUISITION = "acquisition";

    public StagiaireCompetenceTable(Context context, String mail) {
        COMPETENCE_STAGIAIRE_TABLE_NAME = mail;
        dbHelper = new MySQLiteHelper(context);
    }

    public boolean insert(String ccp, String competence, String acquisition) {
        stmt = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COMPETENCE_STAGIAIRE_COLUMN_CCP, ccp);
        values.put(COMPETENCES_STAGIAIRE_COLUMN_COMPETENCES, competence);
        values.put(COMPETENCES_STAGIAIRE_COLUMN_ACQUISITION, acquisition);
        stmt.insert(COMPETENCE_STAGIAIRE_TABLE_NAME, "", values);
        List<String> a = new ArrayList<>();
        Cursor c = stmt.rawQuery(SELECTALL + COMPETENCE_STAGIAIRE_TABLE_NAME, null);
        c.moveToFirst();
        while (!c.isAfterLast()) {
            a.add(c.getString(0));
            a.add(c.getString(1));
            a.add(c.getString(2));
            c.moveToNext();
        }
        c.close();
        if (a.contains(ccp)) {
            return true;
        } else {
            return false;
        }
    }

    public List<String> select(String nameColumn, String nameValues) {
        List<String> a = new ArrayList<>();

        stmt = dbHelper.getWritableDatabase();

        Cursor c = stmt.rawQuery(SELECTALL + COMPETENCE_STAGIAIRE_TABLE_NAME + WHERE + nameColumn + ESPACE + EGALE + ESPACE + QUOTE + nameValues + QUOTE, null);

        c.moveToFirst();
        while (!c.isAfterLast()) {
            a.add(c.getString(0));
            c.moveToNext();
        }
        c.close();

        return a;
    }

//    public List<String> getListCompetences() {
//        stmt = dbHelper.getWritableDatabase();
//        ContentValues values = new ContentValues();
//        ArrayList<String> a = DatabaseAndroid.onSelect(SELECTALL + CompetenceTable.COMPETENCE_TABLE_NAME);
//        String ccp = "";
//        String competence = "";
//        for (int i = 0; i < a.size(); i++) {
//            ccp = a.get(i).substring(0, a.get(i).indexOf(";"));
//            competence = a.get(i).substring(a.get(i).indexOf(";") + 1, a.get(i).length());
//
//        }
//
//        values.put(COMPETENCE_STAGIAIRE_COLUMN_CCP, ccp);
//        values.put(COMPETENCES_STAGIAIRE_COLUMN_COMPETENCES, competence);
//        values.put(COMPETENCES_STAGIAIRE_COLUMN_ACQUISITION, "N");
//        stmt.insert(COMPETENCE_STAGIAIRE_TABLE_NAME, "", values);
//        return DatabaseAndroid.onSelect(SELECTALL + StagiaireCompetenceTable.COMPETENCE_STAGIAIRE_TABLE_NAME);
//    }

    public ArrayList<String> getList() {
        return null;
    }
}
