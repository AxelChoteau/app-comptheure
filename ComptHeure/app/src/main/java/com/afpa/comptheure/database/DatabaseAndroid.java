package com.afpa.comptheure.database;

import static com.afpa.comptheure.database.MySQLiteHelper.stmt;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;

/**
 * Classe abstraite contenant les différentes méthode des bases de données
 */
public abstract class DatabaseAndroid {

    public static boolean onExist(String sql){
        Cursor cursor = stmt.rawQuery(sql, null);
        return cursor.getCount()!= 0;
    }

    /**
     * Méthode pour la requete create
     *
     * @param sql contient la requete
     */
    public static void onCreate(String sql){
        Log.i("titi", sql);
        stmt.execSQL(sql);
    }

    /**
     * Méthode pour la requete insert into
     *
     * @param sql contient la requete
     */
    public static void onInsert(String sql){
        stmt.execSQL(sql);
    }

    /**
     * Méthode pour la requete update
     *
     * @param sql contient la requete
     */
    public static void onUpdate(String sql){
        stmt.execSQL(sql);
    }

    /**
     * Méthode pour la requete select
     *
     * @param sql contient la requête
     * @return une liste de string
     */
    public static ArrayList <String> onSelect (String sql){

        // Déclaration des variables
        ArrayList<String> result = new ArrayList <String> ();
        Cursor cursor = stmt.rawQuery(sql, null);

        StringBuilder sb = new StringBuilder();
        // Boucle pour parcourir chaque ligne de résultat
        for (int i = 0; i < cursor.getCount(); i++){
            cursor.moveToNext();

            // Boucle pour parcourir chaque colonne
            for (int j = 0; j < cursor.getColumnCount(); j++){
                sb.append(cursor.getString(j) + "; ");
            }
            sb.deleteCharAt(sb.lastIndexOf(";"));
            Log.i("Axel", "onSelect: "+sb.toString());
            result.add(sb.toString());
            sb.delete(0, sb.length());
        }
        return result;
    }
}