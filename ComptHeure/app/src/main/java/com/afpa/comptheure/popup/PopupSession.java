package com.afpa.comptheure.popup;

import com.afpa.comptheure.listener.DateFocusListener;
import com.afpa.comptheure.adapter.FormationAdapter;
import android.support.v7.app.AppCompatActivity;
import com.afpa.comptheure.objet.Stagiaire;
import com.afpa.comptheure.objet.Sessions;
import android.view.LayoutInflater;
import android.widget.EditText;
import com.afpa.comptheure.R;
import android.widget.Button;
import android.widget.Toast;
import android.view.View;

public class PopupSession extends Popup {

    private EditText sessionNameEditText = null;
    private Button saveNewSessionButton = null;
    // Click this button to cancel edit user data.
    private Button cancelNewSessionButton = null;

    private FormationAdapter adapter;
    private String formation;

    public PopupSession(AppCompatActivity activity, String title, int layout , FormationAdapter adapter, String formation){
        super(activity,title, layout);
        this.adapter=adapter;
        this.formation=formation;
    }

    @Override
    protected void initPopupViewControls() {
        // Get layout inflater object.
        LayoutInflater layoutInflater = LayoutInflater.from(activity);

        // Inflate the popup dialog from a layout xml file.
        popupView = layoutInflater.inflate(layout, null);

        // Get user input edittext and button ui controls in the popup dialog.
        sessionNameEditText = (EditText) popupView.findViewById(R.id.main_popup_session_start);
        sessionNameEditText.setOnFocusChangeListener(new DateFocusListener(activity));
        saveNewSessionButton = popupView.findViewById(R.id.main_popup_new_session_button);
        saveNewSessionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String sessionName = sessionNameEditText.getText().toString().trim();
                if (!sessionName.equals("")){
                    Sessions.insertSession(formation, sessionName);
                    Stagiaire.createStagiaire(formation, sessionName.replace("/", ""));
                    adapter.notifyDataSetChanged();
                    alertDialog.cancel();
                } else {
                    Toast.makeText(activity.getApplicationContext(),"Erreur dans la saisie de la session", Toast.LENGTH_LONG).show();
                }
            }
        });

        cancelNewSessionButton = popupView.findViewById(R.id.main_popup_cancel_session_button);
        cancelNewSessionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
    }
}