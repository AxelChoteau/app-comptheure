package com.afpa.comptheure.objet;

import static com.afpa.comptheure.constante.ConstanteDataBase.ASCENDANT;
import static com.afpa.comptheure.constante.ConstanteDataBase.ORDER_BY;
import static com.afpa.comptheure.constante.ConstanteDataBase.PARENTHESE_FERM_POINT_VIRGULE;
import static com.afpa.comptheure.database.CompetenceTable.COMPETENCE_COLUMN_COMPETENCE;
import static com.afpa.comptheure.constante.ConstanteDataBase.ESPACE_PARENTHESE_OUVR;
import static com.afpa.comptheure.constante.ConstanteDataBase.PARENTHESE_OUVR_QUOTE;
import static com.afpa.comptheure.constante.ConstanteDataBase.QUOTE_PARENTHESE_FERM;
import static com.afpa.comptheure.constante.ConstanteDataBase.QUOTE_VIRGULE_QUOTE;
import static com.afpa.comptheure.database.CompetenceTable.COMPETENCE_COLUMN_CCP;
import static com.afpa.comptheure.constante.ConstanteDataBase.TEXT_NOT_NULL;
import static com.afpa.comptheure.constante.ConstanteDataBase.CREATE_TABLE;
import static com.afpa.comptheure.constante.ConstanteDataBase.INSERT_INTO;
import static com.afpa.comptheure.constante.ConstanteDataBase.UNDERSCORE;
import static com.afpa.comptheure.constante.ConstanteDataBase.SELECTALL;
import static com.afpa.comptheure.constante.ConstanteDataBase.VIRGULE;
import static com.afpa.comptheure.constante.ConstanteDataBase.SELECT;
import static com.afpa.comptheure.constante.ConstanteDataBase.VALUES;
import static com.afpa.comptheure.constante.ConstanteDataBase.EGALE;
import static com.afpa.comptheure.constante.ConstanteDataBase.QUOTE;
import static com.afpa.comptheure.constante.ConstanteDataBase.SKILL;
import static com.afpa.comptheure.constante.ConstanteDataBase.WHERE;
import static com.afpa.comptheure.constante.ConstanteDataBase.FROM;
import static com.afpa.comptheure.constante.ConstanteDataBase.AND;
import com.afpa.comptheure.database.DatabaseAndroid;

import java.util.ArrayList;

import lombok.Data;

/**
 * Classe permettant la gestion des competences
 *
 * @author Thierry
 */
@Data
public class Competences {

    private String formation;
    private String ccp;
    private String competence;

    /**
     * Constructeur avec deux paramètres
     *
     * @param ccp est le nom du CCP
     * @param competence est le nom de la competence
     */
    public Competences(String ccp, String competence){
        this.ccp = ccp;
        this.competence = competence;
    }

    /**
     * Méthode pour l'ajout d'une compétence dans la base de données
     */
    public static void createCompetence(String formation){
        DatabaseAndroid.onCreate(CREATE_TABLE + formation + UNDERSCORE
                + SKILL + ESPACE_PARENTHESE_OUVR + COMPETENCE_COLUMN_CCP + TEXT_NOT_NULL
                + VIRGULE + COMPETENCE_COLUMN_COMPETENCE + TEXT_NOT_NULL +
                PARENTHESE_FERM_POINT_VIRGULE);
    }

    /**
     * Méthode pour l'ajout d'une compétence dans la base de données
     */
    public static boolean insertCompetence(String formation, String ccp, String comp){

        // Condition d'ajout dans la base de données si données non existante
        if (!DatabaseAndroid.onExist(SELECTALL + formation + UNDERSCORE
                + SKILL + WHERE + COMPETENCE_COLUMN_CCP + EGALE + QUOTE + ccp + QUOTE
                + AND + COMPETENCE_COLUMN_COMPETENCE + EGALE + QUOTE + comp + QUOTE) ){

            // Méthode insert into vers classe abstract
            DatabaseAndroid.onInsert(INSERT_INTO + formation + UNDERSCORE + SKILL
                    + VALUES + PARENTHESE_OUVR_QUOTE + ccp + QUOTE_VIRGULE_QUOTE
                    + comp + QUOTE_PARENTHESE_FERM);
            return true;
        } return false;
    }

    /**
     * Méthode pour le choix des ccp dans la base de données
     */
    public static ArrayList<String> selectAll(String formation){
        return DatabaseAndroid.onSelect(SELECTALL + formation + UNDERSCORE + SKILL
            + ORDER_BY + COMPETENCE_COLUMN_CCP + ASCENDANT);
    }

    /**
     * Méthode pour le choix des ccp dans la base de données
     */
    public static ArrayList<String> selectCcp(String formation){
        return DatabaseAndroid.onSelect(SELECT + COMPETENCE_COLUMN_CCP
                + FROM + formation + UNDERSCORE + SKILL + ORDER_BY
                + COMPETENCE_COLUMN_CCP + ASCENDANT );
    }

    /**
     * Méthode pour le choix de la competence dans la base de données
     */
    public static ArrayList<String> selectCompetence(String formation, String ccp){
        return DatabaseAndroid.onSelect(SELECT + COMPETENCE_COLUMN_COMPETENCE
                + FROM + formation + UNDERSCORE + SKILL + WHERE + COMPETENCE_COLUMN_CCP
                + EGALE + QUOTE + ccp + QUOTE);
    }

    /**
     * Méthode pour la suppresion des doublons de la liste des ccp
     */
    public static ArrayList<String> createListCcp (ArrayList<String>list){
        ArrayList <String> result = new ArrayList<>();
        if (list.size() != 0){
            String ccp = list.get(0);
            result.add(ccp);
            for (int i = 1; i < list.size(); i++) {
                if (!ccp.equalsIgnoreCase(list.get(i))) {
                    ccp = list.get(i);
                    result.add(ccp);
                }
            }
        } return result;
    }
}