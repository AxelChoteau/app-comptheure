package com.afpa.comptheure;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import com.afpa.comptheure.adapter.StagiaireAdapter;
import com.afpa.comptheure.objet.Stagiaire;
import com.afpa.comptheure.popup.PopupStagiaire;

public class SessionActivity extends ActivityAbstract {

    TextView txtSessionName;
    RecyclerView listStagiaire;
    private String sessionName;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.session_activity);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sessionName = getIntent().getStringExtra("session");
        Log.i("thierry", "nom de la session " + sessionName);
        toolbar.setTitle(sessionName);
        txtSessionName = findViewById(R.id.session_tittle);
        txtSessionName.setText(sessionName.substring(0, sessionName.lastIndexOf("_stagiaire")));
        listStagiaire= findViewById(R.id.session_list_stagiaire);
        listStagiaire.setHasFixedSize(true);
        listStagiaire.setLayoutManager(new LinearLayoutManager(this));

        final StagiaireAdapter adapter = new StagiaireAdapter(Stagiaire.selectStagiaire(sessionName), this);
        listStagiaire.setAdapter(adapter);

        FloatingActionButton newStagiaire = findViewById(R.id.fab);
        newStagiaire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new PopupStagiaire(SessionActivity.this, getString(R.string.stagiaire_title), R.layout.new_stagiaire_content, adapter, sessionName).initPopup();
            }
        });

        FloatingActionButton fab2 = findViewById(R.id.fab2);
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SessionActivity.this, GestionActivity.class);
                intent.putExtra("gestion", sessionName);
                startActivity(intent);
            }
        });
    }
}