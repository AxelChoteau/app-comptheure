package com.afpa.comptheure.popup;

import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.afpa.comptheure.MainActivity;
import com.afpa.comptheure.R;
import com.afpa.comptheure.adapter.FormationAdapter;
import com.afpa.comptheure.objet.Competences;
import com.afpa.comptheure.objet.Formation;

public class PopupFormation extends Popup{

    private EditText formationNameEditText = null;

    private Button saveNewFormationButton = null;

    // Click this button to cancel edit user data.

    private Button cancelNewFormationButton = null;

    private FormationAdapter adapter;

    public PopupFormation(AppCompatActivity activity, String title, int layout , FormationAdapter adapter){
        super(activity,title, layout);
        this.adapter=adapter;
    }

    @Override
    protected void initPopupViewControls() {
        // Get layout inflater object.
        LayoutInflater layoutInflater = LayoutInflater.from(activity);

        // Inflate the popup dialog from a layout xml file.
        popupView = layoutInflater.inflate(layout, null);

        // Get user input edittext and button ui controls in the popup dialog.
        formationNameEditText = popupView.findViewById(R.id.main_popup_session_start);
        saveNewFormationButton = popupView.findViewById(R.id.main_popup_new_formation_button);
        saveNewFormationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String formationName = formationNameEditText.getText().toString().trim();
                if (!formationName.equals("") && Formation.insertFormation(formationName)){
                    Competences.createCompetence(formationName);
                    adapter.setFormations(formationName);
                    adapter.notifyDataSetChanged();
                    alertDialog.cancel();
                } else {
                    Toast.makeText(activity.getApplicationContext(), "Erreur dans la saisie de la formation", Toast.LENGTH_LONG).show();
                }
            }
        });

        cancelNewFormationButton = popupView.findViewById(R.id.main_popup_cancel_formation_button);
        cancelNewFormationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
    }
}