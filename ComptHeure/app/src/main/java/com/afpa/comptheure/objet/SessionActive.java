package com.afpa.comptheure.objet;

import lombok.Data;

@Data
public class SessionActive {

    public static String formation_name;
    public static String formation_choix;
    public static String session_date;
    public static String session_choix;
    public static String table_name_stagiaire;
}