package com.afpa.comptheure.adapter;

import android.support.annotation.NonNull;
import android.support.transition.TransitionManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afpa.comptheure.CompetenceActivity;
import com.afpa.comptheure.R;
import com.afpa.comptheure.objet.SessionActive;
import com.afpa.comptheure.objet.Stagiaire;
import com.afpa.comptheure.objet.StagiaireCCP;
import com.afpa.comptheure.viewholders.ViewHolderStagiaireCCP;

import java.util.List;

import lombok.Getter;

public class AdapterStagiaireCCP extends RecyclerView.Adapter<ViewHolderStagiaireCCP>{

    @Getter
    private List<StagiaireCCP> ccpStagiaire;
    private int mExpandedPosition= -1;
    private RecyclerView recyclerView = null;
    private AppCompatActivity activity;

    public AdapterStagiaireCCP(AppCompatActivity activity, List<StagiaireCCP> ccpStagiaire){
        this.activity=activity;
        this.ccpStagiaire=ccpStagiaire;
    }
    @NonNull
    @Override
    public ViewHolderStagiaireCCP onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.stagiaire_competence_ccp_holder, parent, false);
        return new ViewHolderStagiaireCCP(v, this, activity);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderStagiaireCCP viewHolder, int position) {
        final String ccpName = ccpStagiaire.get(position).getName();
//        SessionActive.formation_name = formations.get(position).toUpperCase().trim();
        viewHolder.bind(ccpStagiaire.get(position));
        viewHolder.getSubItem().setVisibility(View.GONE);

        final boolean isExpanded = position==mExpandedPosition;
        final int pos=position;

        viewHolder.getSubItem().setVisibility(isExpanded?View.VISIBLE:View.GONE);
        viewHolder.getImgExpand().setImageResource(isExpanded? R.drawable.icons8_collapse_arrow_36:R.drawable.icons8_expand_arrow_36);
        viewHolder.itemView.setActivated(isExpanded);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mExpandedPosition = isExpanded ? -1:pos;
//                TransitionManager.beginDelayedTransition(recyclerView);
                notifyDataSetChanged();
//                SessionActive.formation_choix = ccpStagiaire.get(pos).getName().trim();
            }
        });
    }

    @Override
    public int getItemCount() {
        return ccpStagiaire.size();
    }
}
