package com.afpa.comptheure;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AppCompatActivity;

import lombok.Getter;

public class GestionPagerAdapter extends FragmentPagerAdapter {

    final int PAGE_COUNT = 2;
    private String tabTitles[] = new String[] { "Presence", "Retards"};
    private GestionActivity context;
    @Getter
private RetardActivity retardActivity;
    public GestionPagerAdapter(FragmentManager fm, GestionActivity context) {
        super(fm);
        this.context = context;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return PresenceActivity.newInstance(context);
            case 1:
                retardActivity=RetardActivity.newInstance(context);
                return retardActivity;
        }
        return null;

    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }

}
