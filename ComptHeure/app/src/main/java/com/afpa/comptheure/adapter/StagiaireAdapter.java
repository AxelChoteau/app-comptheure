package com.afpa.comptheure.adapter;

import com.afpa.comptheure.viewholders.StagiaireViewHolder;
import android.support.v7.widget.RecyclerView;
import com.afpa.comptheure.SessionActivity;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.afpa.comptheure.R;
import android.view.View;
import java.util.List;
import lombok.Getter;

public class StagiaireAdapter extends RecyclerView.Adapter<StagiaireViewHolder>{
        //    private List<Formation> formations;
    @Getter
        private List<String> stagiaires;
        private int mExpandedPosition= -1;
        private RecyclerView recyclerView = null;
        @Getter
        private SessionActivity activity;

        public StagiaireAdapter(List<String> stagiaires, SessionActivity activity){
            this.activity=activity;
            this.stagiaires=stagiaires;
        }

        @NonNull
        @Override
        public StagiaireViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.session_stagiaire_info, parent, false);
            return new StagiaireViewHolder(this, v);
        }

        @Override
        public void onBindViewHolder(@NonNull  StagiaireViewHolder viewHolder, int position) {
            String[] data = stagiaires.get(position).split(";");
            viewHolder.bind(data);
            viewHolder.getSubItem().setVisibility(View.GONE);

            final boolean isExpanded = position==mExpandedPosition;
            final int pos=position;

            viewHolder.getSubItem().setVisibility(isExpanded?View.VISIBLE:View.GONE);
            viewHolder.getImgExpand().setImageResource(isExpanded? R.drawable.icons8_collapse_arrow_36:R.drawable.icons8_expand_arrow_36);
            viewHolder.itemView.setActivated(isExpanded);

            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mExpandedPosition = isExpanded ? -1:pos;

                    notifyDataSetChanged();
                }
            });
        }

        @Override
        public int getItemCount() {
            return stagiaires.size();
        }

        @Override
        public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
            super.onAttachedToRecyclerView(recyclerView);

            this.recyclerView = recyclerView;
        }

        public void setStagiaires(String stagiaire){
            stagiaires.add(stagiaire);
        }
    }