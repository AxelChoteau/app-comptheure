package com.afpa.comptheure.tools;

public class Format {

    public final static String SPACE = " ";
    public final static String POINT = ".";
    public final static String AROBASE = "@";
    public final static String QUOTE = "'";
    public final static String GUILLEMET = "\"";

    public final static String SPACE_TXT = "space";
    public final static String POINT_TXT = "point";
    public final static String AROBASE_TXT = "arobase";
    public final static String QUOTE_TXT = "quote";
    public final static String GUILLEMET_TXT = "guillemet";


    public static String formateString(String str){
        return str.replace(SPACE , SPACE_TXT)
                .replace(POINT , POINT_TXT)
                .replace(AROBASE , AROBASE_TXT)
                .replace(QUOTE , QUOTE_TXT)
                .replace(GUILLEMET , GUILLEMET_TXT);
    }

    public static String deFormateString(String str){
        return str.replace(SPACE_TXT , SPACE)
                .replace(POINT_TXT , POINT)
                .replace(AROBASE_TXT , AROBASE)
                .replace(QUOTE_TXT , QUOTE )
                .replace(GUILLEMET_TXT , GUILLEMET);
    }


}
