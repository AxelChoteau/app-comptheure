package com.afpa.comptheure.popup;

import android.support.v7.app.AppCompatActivity;

import com.afpa.comptheure.CompetenceActivity;
import com.afpa.comptheure.adapter.CCPAdapter;
import com.afpa.comptheure.objet.Competences;
import android.view.LayoutInflater;
import android.widget.EditText;
import com.afpa.comptheure.R;
import android.widget.Button;
import android.view.View;

public class PopupCompetence extends Popup {

    private EditText ccpNameEditText;
    private EditText competenceNameEditText;
    private Button saveNewCompetenceButton;
    private Button cancelNewCompetenceButton;
    private CCPAdapter adapter;
    private String formation;

    public PopupCompetence(AppCompatActivity activity, String title, int layout, CCPAdapter adapter, String formation){
        super(activity,title,layout);
        this.formation = formation;
        this.adapter=adapter;
    }

    @Override
    protected void initPopupViewControls() {

        // Get layout inflater object.
        LayoutInflater layoutInflater = LayoutInflater.from(activity);

        // Inflate the popup dialog from a layout xml file.
        popupView = layoutInflater.inflate(layout, null);

        // Get user input edittext and button ui controls in the popup dialog.
        ccpNameEditText = popupView.findViewById(R.id.new_competence_ccp_name);
        competenceNameEditText=popupView.findViewById(R.id.new_competence_competence_name);

        saveNewCompetenceButton = popupView.findViewById(R.id.new_competence_button);
        saveNewCompetenceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String formationName = ccpNameEditText.getText().toString();
//                String competenceName = competenceNameEditText.getText().toString();
                Competences.insertCompetence(formation,ccpNameEditText.getText().toString(), competenceNameEditText.getText().toString());
                adapter.setCcp(Competences.createListCcp(Competences.selectCcp(((CompetenceActivity) activity).getFormationName())));
                adapter.notifyDataSetChanged();
                alertDialog.cancel();
            }
        });

        cancelNewCompetenceButton = popupView.findViewById(R.id.new_competence_cancel_button);
        cancelNewCompetenceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
    }
}