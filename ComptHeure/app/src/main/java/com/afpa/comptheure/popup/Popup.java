package com.afpa.comptheure.popup;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.afpa.comptheure.MainActivity;
import com.afpa.comptheure.R;

public abstract class Popup {

    protected AppCompatActivity activity;
    protected View popupView;
    protected String title;
    protected int layout;
    protected AlertDialog alertDialog;

    protected Popup(AppCompatActivity activity, String title, int layout){
        this.activity=activity;
        this.title=title;
        this.layout=layout;
    }

    public void initPopup(){
        // Create a AlertDialog Builder.
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
        // Set title, icon, can not cancel properties.
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setIcon(R.mipmap.logo);
        alertDialogBuilder.setCancelable(false);

        // Init popup dialog view and it's ui controls.
        initPopupViewControls();

        // Set the inflated layout view object to the AlertDialog builder.
        alertDialogBuilder.setView(popupView);

        // Create AlertDialog and show.
        alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    protected abstract void initPopupViewControls();


}
