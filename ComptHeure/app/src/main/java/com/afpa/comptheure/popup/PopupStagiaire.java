package com.afpa.comptheure.popup;

import com.afpa.comptheure.adapter.StagiaireAdapter;
import android.support.v7.app.AppCompatActivity;

import com.afpa.comptheure.database.DatabaseAndroid;
import com.afpa.comptheure.objet.Competences;
import com.afpa.comptheure.objet.Stagiaire;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.Button;
import com.afpa.comptheure.R;
import android.view.View;
import android.widget.Toast;

import java.util.regex.Pattern;

public class PopupStagiaire extends Popup {

    private Button cancelNewStagiaireButton;
    private Button saveNewStagiaireButton;
    private StagiaireAdapter adapter;
    private String nameTable;
    private EditText txtFirstName;
    private EditText txtName;
    private EditText txtMail;

    public PopupStagiaire(AppCompatActivity activity, String title, int layout, StagiaireAdapter adapter, String nameTable){
        super(activity,title,layout);
        this.nameTable = nameTable;
        this.adapter =adapter;

    }

    @Override
    protected void initPopupViewControls() {
// Get layout inflater object.
        LayoutInflater layoutInflater = LayoutInflater.from(activity);

        // Inflate the popup dialog from a layout xml file.
        popupView = layoutInflater.inflate(layout, null);

        // Get user input edittext and button ui controls in the popup dialog.
        txtName = popupView.findViewById(R.id.new_stagiaire_name);
        txtFirstName = popupView.findViewById(R.id.new_stagiaire_firstname);
        txtMail = popupView.findViewById(R.id.new_stagiaire_email);
        saveNewStagiaireButton = popupView.findViewById(R.id.new_stagiaire_button);
        saveNewStagiaireButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String name = txtName.getText().toString().trim();
                String firstName = txtFirstName.getText().toString().trim();
                String mail=txtMail.getText().toString().trim();
                if(!name.matches("[a-zA-Z 'éèêïü-]+")||!firstName.matches("[a-zA-Z 'éèêïü-]+")|| !Pattern.matches("\\b[\\w.%-]+@[-.\\w]+\\.[A-Za-z]{2,4}\\b", mail)){
                    Toast.makeText(activity.getApplicationContext(), "Erreur dans la saisie, impossible de valider la création", Toast.LENGTH_LONG).show();
                    return;
                }

                if(Stagiaire.insertStagiaire(nameTable, name,
                        firstName, mail)){
                    Stagiaire.createLate(txtMail.getText().toString().trim().replace("@", "arobase").replace(".", "point"));
                    Stagiaire.createSkill(txtMail.getText().toString().trim().replace("@", "arobase").replace(".", "point"));
                    Stagiaire.createListCompetences(Competences.selectAll(nameTable.substring(0, nameTable.indexOf("_")))
                    , txtMail.getText().toString().trim().replace("@", "arobase").replace(".", "point"));
                    adapter.setStagiaires(name+";"+firstName+";"+mail);
                    adapter.notifyDataSetChanged();
                    alertDialog.cancel();
                } else {
                    Toast.makeText(activity.getApplicationContext(), "Impossible d'avoir deux adresses mail identiques dans une session", Toast.LENGTH_LONG).show();
                }

            }
        });

        cancelNewStagiaireButton = popupView.findViewById(R.id.new_stagiaire_cancel_button);
        cancelNewStagiaireButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
    }
}