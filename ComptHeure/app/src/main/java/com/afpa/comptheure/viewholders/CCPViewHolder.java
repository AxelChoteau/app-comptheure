package com.afpa.comptheure.viewholders;

import com.afpa.comptheure.listener.TypeFormationListener;
import android.support.v7.widget.LinearLayoutManager;
import com.afpa.comptheure.adapter.CompetenceAdapter;
import android.support.v7.widget.RecyclerView;
import com.afpa.comptheure.CompetenceActivity;
import com.afpa.comptheure.adapter.CCPAdapter;
import com.afpa.comptheure.objet.Competences;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.afpa.comptheure.R;
import android.view.View;
import lombok.Getter;

@Getter
public class CCPViewHolder extends RecyclerView.ViewHolder {
    private TextView txtCCPName;
    private LinearLayout subItem;
    private RecyclerView listCompetence;
    private CCPAdapter adapter;
    private CompetenceActivity activity;
    private ImageView imgExpand;

    public CCPViewHolder(View v, CCPAdapter adapter, CompetenceActivity activity){
        super(v);
        this.adapter=adapter;
        this.activity=activity;
        txtCCPName=itemView.findViewById(R.id.competence_ccp_name);
        subItem=itemView.findViewById(R.id.competence_subitem);
        listCompetence=itemView.findViewById(R.id.competence_competence_list);
        itemView.setOnClickListener(new TypeFormationListener(this.adapter));
        imgExpand=itemView.findViewById(R.id.competence_expand);
    }

    public void bind(String ccpName){

        txtCCPName.setText(ccpName);
        listCompetence.setHasFixedSize(true);
//        new Formation(formation);
//        st = new SessionTable(activity);
//        this.formation = formation;
        listCompetence.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
        CompetenceAdapter cAdapter = new CompetenceAdapter(activity, Competences.selectCompetence(activity.getFormationName(), ccpName.trim()));
        listCompetence.setAdapter(cAdapter);
    }
}