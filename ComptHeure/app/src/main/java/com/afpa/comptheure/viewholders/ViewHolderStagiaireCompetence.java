package com.afpa.comptheure.viewholders;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.afpa.comptheure.R;
import com.afpa.comptheure.adapter.AdapterStagiaireCompetence;

import lombok.Getter;

@Getter
public class ViewHolderStagiaireCompetence extends RecyclerView.ViewHolder{

    private AdapterStagiaireCompetence adapter;
    private AppCompatActivity activity;

    private TextView txtCompetenceName;

    private RadioGroup radioGroup;

    private RadioButton btnAcquis;
    private RadioButton btnEnCours;
    private RadioButton btnNonAcquis;

    public ViewHolderStagiaireCompetence(View v, AdapterStagiaireCompetence adapter, final AppCompatActivity activity){
        super(v);
        this.adapter=adapter;
        this.activity=activity;
        txtCompetenceName=itemView.findViewById(R.id.stagiaire_competence_name);

        radioGroup=itemView.findViewById(R.id.radioGroup);

        btnAcquis=itemView.findViewById(R.id.stagiaire_competence_acquis);
        btnAcquis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewHolderStagiaireCompetence.this.adapter.getCompetences().get(getAdapterPosition()).setEtat("a");
            }
        });
        btnEnCours=itemView.findViewById(R.id.stagiaire_competence_encours);
        btnEnCours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewHolderStagiaireCompetence.this.adapter.getCompetences().get(getAdapterPosition()).setEtat("e");
            }
        });
        btnNonAcquis=itemView.findViewById(R.id.stagiaire_competence_nonacquis);
        btnNonAcquis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewHolderStagiaireCompetence.this.adapter.getCompetences().get(getAdapterPosition()).setEtat("n");
            }
        });
    }
}
