package com.afpa.comptheure.viewholders;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afpa.comptheure.ActivityStagiaireCompetence;
import com.afpa.comptheure.MainActivity;
import com.afpa.comptheure.R;
import com.afpa.comptheure.SessionActivity;
import com.afpa.comptheure.adapter.SessionAdapter;
import com.afpa.comptheure.adapter.StagiaireAdapter;
import com.afpa.comptheure.database.SessionTable;
import com.afpa.comptheure.listener.DateFocusListener;
import com.afpa.comptheure.listener.TypeFormationListener;
import com.afpa.comptheure.objet.Formation;
import com.afpa.comptheure.objet.Stagiaire;
import com.afpa.comptheure.tools.Graph;
import com.afpa.comptheure.tools.Retard;
import com.github.mikephil.charting.charts.PieChart;

import java.text.ParseException;

import lombok.Getter;


public class StagiaireViewHolder extends RecyclerView.ViewHolder {

    @Getter
    private ConstraintLayout subItem;

    private TextView txtStagiaireName;
    private TextView txtStagiareMail;
    private TextView txtStagiaireAbsence;
    private TextView txtStagiaireRetard;
    @Getter
    private ImageView imgExpand;
    private Button btnUpdateCompetence;
    private StagiaireAdapter adapter;
private PieChart ccpPieChart;
    public StagiaireViewHolder(final StagiaireAdapter adapter, View v){
        super(v);
        this.adapter=adapter;

        txtStagiaireName= itemView.findViewById(R.id.session_stagiaire_name);
        subItem= itemView.findViewById(R.id.session_subitem);

        txtStagiareMail=itemView.findViewById(R.id.session_stagiaire_email);
        txtStagiaireAbsence=itemView.findViewById(R.id.session_stagiaire_absence);
        txtStagiaireRetard=itemView.findViewById(R.id.session_stagiaire_retard);
        imgExpand=itemView.findViewById(R.id.imageView);
        btnUpdateCompetence=itemView.findViewById(R.id.session_stagiaire_update_button);


        ccpPieChart=itemView.findViewById(R.id.session_stagiaire_piechart);


        btnUpdateCompetence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StagiaireViewHolder.this.adapter.getActivity().getApplicationContext(), ActivityStagiaireCompetence.class);
                String stagiaire = adapter.getStagiaires().get(getAdapterPosition());
                intent.putExtra("stagiaire",stagiaire);
                StagiaireViewHolder.this.adapter.getActivity().startActivity(intent);
            }
        });
}

    public void bind(String[] stagiaire){
        txtStagiaireName.setText(stagiaire[0] + " " + stagiaire[1]);
        txtStagiareMail.setText(stagiaire[2].trim());
        txtStagiaireAbsence.setText("");
        String[] retardAbsense;
        try {
            retardAbsense= Retard.getRetard(stagiaire[2].replace("@", "arobase").replace(".", "point").trim()).split(";");
            txtStagiaireAbsence.setText("Absences dans le mois (en demi journée) : "+retardAbsense[0]);
            txtStagiaireRetard.setText("Retards dans le mois (en heure) : " + retardAbsense[1]);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Graph.drawGraph(adapter.getActivity(), ccpPieChart, Stagiaire.selectSkill(stagiaire[2].replace(".", "point").replace("@","arobase").trim()));

    }

}
