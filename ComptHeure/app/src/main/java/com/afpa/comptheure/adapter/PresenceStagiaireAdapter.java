package com.afpa.comptheure.adapter;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afpa.comptheure.GestionActivity;
import com.afpa.comptheure.R;
import com.afpa.comptheure.objet.Stagiaire;
import com.afpa.comptheure.viewholders.PresenceStagiaireViewHolder;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class PresenceStagiaireAdapter extends RecyclerView.Adapter<PresenceStagiaireViewHolder>{

    private GestionActivity activity;

    @Getter
    private List<Stagiaire> stagiaires;

    private RecyclerView recyclerView;

    @Getter
    @Setter
    private boolean allSelected=true;

    public PresenceStagiaireAdapter(GestionActivity activity){
        this.activity=activity;
        this.stagiaires=this.activity.getListStagiaires();
    }

    @NonNull
    @Override
    public PresenceStagiaireViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.precence_stagiaire_list_holder, parent, false);
        return new PresenceStagiaireViewHolder(v, this);
    }

    @Override
    public void onBindViewHolder(@NonNull PresenceStagiaireViewHolder viewHolder, int position) {
        Stagiaire stagiaire=stagiaires.get(position);
        viewHolder.getStagiaireName().setText(stagiaire.getName()+" "+stagiaire.getFirstname());
        stagiaire.setSelected(isAllSelected());
        viewHolder.getSelectStagiaire().setChecked(stagiaire.isSelected());

    }

    @Override
    public int getItemCount() {
        return stagiaires.size();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

        this.recyclerView = recyclerView;
    }

}
