package com.afpa.comptheure.constante;

public class ConstanteDataBase {

  //types
    public static final String TEXT_NOT_NULL = " TEXT NOT NULL";
    public static final String INT_NOT_NULL = " INTEGER NOT NULL";
    public static final String INTEGER_ = " INTEGER";
    public static final String PRIMARY_KEY = " PRIMARY KEY";
    public static final String TEXT_PRIMARY_KEY = " TEXT PRIMARY KEY";
    public static final String STAGIAIRE = "stagiaire";
    public static final String SKILL = "competences";
    public static final String LATE = "retard";

    // caractères speciaux
    public static final String ESPACE_PARENTHESE_OUVR = " (";
    public static final String PARENTHESE_FERM = ")";
    public static final String PARENTHESE_FERM_POINT_VIRGULE = ");";
    public static final String UNDERSCORE = "_";
    public static final String VIRGULE = ",";
    public static final String ETOILE = "* ";
    public static final String ESPACE = " ";
    public static final String EGALE = " = ";
    public static final String QUOTE = "'";
    public static final String PARENTHESE_OUVR_QUOTE = "('";
    public static final String QUOTE_PARENTHESE_FERM = "')";
    public static final String QUOTE_VIRGULE_QUOTE = "','";
    public static final String POINT_VIRGULE = ";";
    public static final String PARENTHESE_OUVR = "(";

    // requete
    public static final String SELECT = "SELECT ";
    public static final String SELECTALL = "SELECT * FROM ";
    public static final String INSERT_INTO = "INSERT INTO ";
    public static final String ORDER_BY = " ORDER BY ";
    public static final String ASCENDANT = " ASC ";
    public static final String DESCENDANT = " DESC ";
    public static final String FROM = " FROM ";
    public static final String WHERE = " WHERE ";
    public static final String VALUES = " VALUES ";
    public static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS ";
    public static final String UPDATE = "UPDATE ";
    public static final String SET = " SET ";
    public static final String AND = " AND ";
}