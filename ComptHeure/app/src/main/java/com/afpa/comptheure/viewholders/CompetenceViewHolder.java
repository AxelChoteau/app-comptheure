package com.afpa.comptheure.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.afpa.comptheure.R;

import lombok.Getter;

public class CompetenceViewHolder extends RecyclerView.ViewHolder {
    @Getter
    private TextView txtCompetenceName;
    public CompetenceViewHolder(View v){
        super(v);
        txtCompetenceName=itemView.findViewById(R.id.competence_competence_name);
    }
}
