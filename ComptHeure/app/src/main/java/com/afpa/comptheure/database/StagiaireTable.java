package com.afpa.comptheure.database;

import com.afpa.comptheure.constante.ConstanteDataBase;
import com.afpa.comptheure.objet.SessionActive;
import android.database.sqlite.SQLiteDatabase;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import java.util.ArrayList;
import java.util.List;

public class StagiaireTable {

    private MySQLiteHelper maBaseSQLite;
    private SQLiteDatabase db;
    private Context context;

    public static final String STAGIAIRE_COLUMN_NAME = "Nom";
    public static final String STAGIAIRE_COLUMN_FIRSTNAME = "Prenom";
    public static final String STAGIAIRE_COLUMN_MAIL = "Email";
    public static String CREATE_STAGIAIRE_TABLE_NAME;
    public static String STAGIAIRE_SELECT_All = ConstanteDataBase.SELECTALL
            + SessionActive.table_name_stagiaire
            + ConstanteDataBase.ORDER_BY + STAGIAIRE_COLUMN_NAME
            + ConstanteDataBase.VIRGULE + STAGIAIRE_COLUMN_FIRSTNAME
            + ConstanteDataBase.ASCENDANT;

    public StagiaireTable(Context context) {
        this.context = context;
        maBaseSQLite = new MySQLiteHelper(context);
        open();
    }

    public void open() {
        //on ouvre la table en lecture/écriture
        db = maBaseSQLite.getWritableDatabase();
    }

    public void close() {
        //on ferme l'accès à la BDD
        db.close();
    }

    public boolean insert(String name, String firstName, String mail) {
        if (!DatabaseAndroid.onExist(ConstanteDataBase.SELECTALL + SessionActive.table_name_stagiaire + ConstanteDataBase.WHERE
            + StagiaireTable.STAGIAIRE_COLUMN_MAIL + ConstanteDataBase.EGALE + ConstanteDataBase.QUOTE
            + mail + ConstanteDataBase.QUOTE)){
            ContentValues values = new ContentValues();
            values.put(STAGIAIRE_COLUMN_NAME, name);
            values.put(STAGIAIRE_COLUMN_FIRSTNAME, firstName);
            values.put(STAGIAIRE_COLUMN_MAIL, mail);
            new StagiaireCompetenceTable( context , mail);
            return db.insert(SessionActive.table_name_stagiaire, null, values) > 0;
        }return false;
    }

    public ArrayList<String> getList() {
        ArrayList<String> list = new ArrayList<>();
        list.add("najim;jaadar;naji@hotmail.fr");
        list.add("thierry;titi;titi@hotmail.fr");
        return list;
    }
}