package com.afpa.comptheure.tools;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.Legend.LegendPosition;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import android.util.Log;
import java.util.List;


/**
 * Classe permettant de gérer la création d'un graphique
 */
public class Graph {

    /**
     * Méthode permettant la construction du graphe
     *
     * @param activity contient l'activité de la page
     */
    public static void drawGraph(AppCompatActivity activity, PieChart pieChart, List<String> ccp) {

        // Création des variables
        ArrayList<String> list = getCCP(ccp);

        // Déclaration des paramètes du graphe
        pieChart.setHoleColor(Color.TRANSPARENT);
        pieChart.setUsePercentValues(true);
        pieChart.setDrawHoleEnabled(true);
        pieChart.getDescription().setEnabled(false);
        pieChart.setExtraOffsets(10, 10, 10, 10);
        pieChart.setRotationEnabled(false);
        //pieChart.setDragDecelerationFrictionCoef(0f);
        pieChart.setTransparentCircleAlpha(0);
        pieChart.setDrawEntryLabels(false);
        Legend l = pieChart.getLegend();
        l.setFormSize(12f);
        l.setForm(Legend.LegendForm.CIRCLE);


        //l.setPosition(LegendPosition.BELOW_CHART_LEFT);
        l.setTextSize(0.5f);
        l.setWordWrapEnabled(false);
        //l.setMaxSizePercent(0.95f);
        l.setDrawInside(true);
        l.setTextSize(10f);
        l.setTextColor(Color.BLACK);
        l.setWordWrapEnabled(false);

       // l.setPosition(LegendPosition.BELOW_CHART_LEFT);
        //l.setFormLineWidth(10f);

        // Déclaration des valeurs du graphe
        ArrayList<PieEntry> entry = new ArrayList<>();

        // Boucle pour l'ajout des données dans le graphe
        entry.add( new PieEntry(Float.parseFloat(list.get(0).substring(0, list.get(0).indexOf(";")).trim())
                - Float.parseFloat(list.get(0).substring(list.get(0).indexOf(";") + 1).trim()), ""));

        float total = 0;
        for (int i = 1; i < list.size(); i++) {
            String a = list.get(i);
            Float pourcent = 0.0f;
            pourcent = Float.parseFloat(a.substring(a.lastIndexOf(";") + 1).trim());

            String nom = a.substring(0, a.indexOf(";")).trim();
            if (pourcent > 0)
            entry.add( new PieEntry(pourcent, "CCP" + (i )));
            total += pourcent;
        }


        //Ajout total central
        Float total1 = total * 100 / ccp.size();
        pieChart.setCenterText(total1.intValue() + " %");
        pieChart.setCenterTextSize(30f);

        // Ajout des valeurs dans le graphe, et choix des couleurs
        PieDataSet dataset = new PieDataSet(entry, "");
        dataset.setSliceSpace(3f);
        dataset.setSelectionShift(3f);
        dataset.setColors(listColor());




        // Paramètres de la police de valeur
        PieData data = new PieData(dataset);
        data.setValueTextSize(10f);
        data.setValueTextColor(Color.WHITE);

        pieChart.setData(data);

        pieChart.setUsePercentValues(true);

        pieChart.invalidate();

    }

    /**
     * Méthode pour la mise en forme des données pour la construction du graphe
     *
     * @return une list de string
     */
    private static ArrayList<String> getCCP(List<String> list) {

        // Déclaration des variables
        int comptComp = 1;
        int comptAcquis = 0;
        int comptVal = 0;
        String ccp = "";
        ArrayList<String> result = new ArrayList<String>();
//        ArrayList <String> list = getList();
        StringBuilder sb = new StringBuilder();
        result.add(list.size() + "");

        // Boucle pour le comptage des CCP
        for (String a : list) {
            Log.i("axel", " texte : " + a);
            // Condition pour la premiere ligne
            if (ccp.equalsIgnoreCase("")) {
                sb.append(a.substring(0, a.indexOf(";")));
                ccp = sb.toString();
            }

            // Condition si le ccp précédent et l'actuel sont différents
            else if (!a.substring(0, a.indexOf(";")).equalsIgnoreCase(ccp)) {
                sb.append(";" + comptComp + ";" + comptVal);
                result.add(sb.toString());
                sb.delete(0, sb.length());
                sb.append(a.substring(0, a.indexOf(";")));
                ccp = sb.toString();
                comptComp = 1;
                comptAcquis += comptVal;
                comptVal = 0;

            } else {
                comptComp++;

            }
            if (isAcquired(a)) {
                comptVal++;
            }
        }

        comptAcquis += comptVal;
        sb.append(";" + comptComp + ";" + comptVal);
        result.add(sb.toString());
        result.set(0, list.size() + ";" + comptAcquis);
        return result;
    }

    /**
     * Méthode permettant de vérifier l'acquisition
     *
     * @param str est la ligne de données du stagiaire
     * @return un boolean
     */
    private static boolean isAcquired(String str) {
        return str.substring(str.lastIndexOf(";") + 1).trim().equalsIgnoreCase("a");
    }

    private static ArrayList<Integer> listColor() {
        ArrayList<Integer> listColor = new ArrayList<>();
        listColor.add(Color.TRANSPARENT);
        for (int i = 0; i < ColorTemplate.COLORFUL_COLORS.length; i++) {
            listColor.add(ColorTemplate.COLORFUL_COLORS[i]);
        }
        return listColor;
    }
}