package com.afpa.comptheure.objet;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class StagiaireCCP {
    private String name;
    private List<StagiaireCompetence> competences= new ArrayList<>();

    public StagiaireCCP(String name){
        this.name=name;
    }

    public void addCompetence(StagiaireCompetence competence){
        competences.add(competence);
    }
}