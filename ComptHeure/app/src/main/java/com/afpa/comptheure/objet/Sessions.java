package com.afpa.comptheure.objet;

import android.util.Log;

import static com.afpa.comptheure.constante.ConstanteDataBase.AND;
import static com.afpa.comptheure.constante.ConstanteDataBase.PARENTHESE_FERM_POINT_VIRGULE;
import static com.afpa.comptheure.constante.ConstanteDataBase.ESPACE_PARENTHESE_OUVR;
import static com.afpa.comptheure.constante.ConstanteDataBase.PARENTHESE_OUVR_QUOTE;
import static com.afpa.comptheure.constante.ConstanteDataBase.QUOTE_PARENTHESE_FERM;
import static com.afpa.comptheure.constante.ConstanteDataBase.QUOTE_VIRGULE_QUOTE;
import static com.afpa.comptheure.constante.ConstanteDataBase.PARENTHESE_FERM;
import static com.afpa.comptheure.constante.ConstanteDataBase.POINT_VIRGULE;
import static com.afpa.comptheure.database.SessionTable.SESSION_COLUMN_DATE;
import static com.afpa.comptheure.database.SessionTable.SESSION_COLUMN_NAME;
import static com.afpa.comptheure.constante.ConstanteDataBase.TEXT_NOT_NULL;
import static com.afpa.comptheure.database.SessionTable.SESSION_TABLE_NAME;
import static com.afpa.comptheure.constante.ConstanteDataBase.CREATE_TABLE;
import static com.afpa.comptheure.constante.ConstanteDataBase.INSERT_INTO;
import static com.afpa.comptheure.constante.ConstanteDataBase.PRIMARY_KEY;
import static com.afpa.comptheure.constante.ConstanteDataBase.SELECTALL;
import static com.afpa.comptheure.constante.ConstanteDataBase.VIRGULE;
import static com.afpa.comptheure.constante.ConstanteDataBase.SELECT;
import static com.afpa.comptheure.constante.ConstanteDataBase.VALUES;
import static com.afpa.comptheure.constante.ConstanteDataBase.QUOTE;
import static com.afpa.comptheure.constante.ConstanteDataBase.EGALE;
import static com.afpa.comptheure.constante.ConstanteDataBase.WHERE;
import static com.afpa.comptheure.constante.ConstanteDataBase.FROM;
import com.afpa.comptheure.database.DatabaseAndroid;
import com.afpa.comptheure.database.SessionTable;
import java.util.ArrayList;
import lombok.Data;

/**
 * Classe permettant la gestion des sessions
 *
 * @author Thierry
 */
@Data
public class Sessions {

    private String formation;
    private String date;

    /**
     * Constructeur avec un paramètre
     *
     * @param date est la date de début de formation
     */
    public Sessions (String formation, String date){
        this.formation = formation;
        this.date = date;
    }

    /**
     * Méthode permettant la création de la table
     */
    public static void createSession(){
        DatabaseAndroid.onCreate(CREATE_TABLE + SESSION_TABLE_NAME + ESPACE_PARENTHESE_OUVR
                + SESSION_COLUMN_NAME + TEXT_NOT_NULL + VIRGULE + SESSION_COLUMN_DATE
                + TEXT_NOT_NULL + VIRGULE + PRIMARY_KEY + ESPACE_PARENTHESE_OUVR
                + SESSION_COLUMN_NAME + VIRGULE + SESSION_COLUMN_DATE + PARENTHESE_FERM
                + PARENTHESE_FERM_POINT_VIRGULE);
    }

    /**
     * Méthode pour l'ajout de la session dans la base de données
     *
     * @param formation contient le nom de la formation
     * @param date contient la date de la session
     */
    public static boolean insertSession(String formation, String date){
        // Condition d'ajout dans la base de données si données non existante
        if (!DatabaseAndroid.onExist(SELECTALL + SESSION_TABLE_NAME + WHERE
                + SESSION_COLUMN_NAME + EGALE + QUOTE + formation + QUOTE + AND
                + SESSION_COLUMN_DATE + EGALE + QUOTE + date + QUOTE)){

            // Méthode insert into vers classe abstract
            DatabaseAndroid.onInsert(INSERT_INTO + SessionTable.SESSION_TABLE_NAME
                + VALUES + PARENTHESE_OUVR_QUOTE + formation + QUOTE_VIRGULE_QUOTE
                    + date + QUOTE_PARENTHESE_FERM);
            return true;
        } return false;
    }

    /**
     * Méthode pour le choix de la session dans la base de données
     */
    public static ArrayList<String> selectSession(String formation){
        return DatabaseAndroid.onSelect(SELECT + SESSION_COLUMN_DATE
                + FROM + SESSION_TABLE_NAME + WHERE + SESSION_COLUMN_NAME
                + EGALE + QUOTE + formation + QUOTE + POINT_VIRGULE);
    }
}