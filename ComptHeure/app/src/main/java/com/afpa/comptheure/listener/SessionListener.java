package com.afpa.comptheure.listener;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.afpa.comptheure.MainActivity;
import com.afpa.comptheure.SessionActivity;
import com.afpa.comptheure.constante.ConstanteDataBase;
import com.afpa.comptheure.database.StagiaireTable;
import com.afpa.comptheure.objet.SessionActive;

public class SessionListener implements View.OnClickListener{

    MainActivity activity;
    String sessionName;

    public SessionListener(MainActivity activity, String sessionName){
        this.activity=activity;
        this.sessionName=sessionName;
    }

    @Override
    public void onClick(View v) {
        Intent intent=new Intent(activity.getApplicationContext(), SessionActivity.class);
//        SessionActive.session_choix = sessionName.trim().replace("/", "");
//        SessionActive.table_name_stagiaire = SessionActive.formation_choix + ConstanteDataBase.UNDERSCORE
//                + SessionActive.session_choix + ConstanteDataBase.UNDERSCORE + ConstanteDataBase.STAGIAIRE;
//        Log.i("Axel", "onClick: " + SessionActive.table_name_stagiaire );
        intent.putExtra("session", activity.getFormationName().trim()+"_"+sessionName.trim().replace("/", "")+ConstanteDataBase.UNDERSCORE + ConstanteDataBase.STAGIAIRE);

        activity.startActivity(intent);
    }
}