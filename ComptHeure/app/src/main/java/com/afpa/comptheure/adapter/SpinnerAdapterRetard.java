package com.afpa.comptheure.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.afpa.comptheure.R;

import java.util.List;

public class SpinnerAdapterRetard extends ArrayAdapter<String> {

    private final LayoutInflater mInflater;
    private final Context mContext;
    private final List<String> retard;
    private final int mResource;

    public SpinnerAdapterRetard(Context context, int resource,
                                List objects){
        super(context, resource, 0, objects);
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        retard = objects;

    }

    @Override
    public View getDropDownView(int position,  View convertView,
                                 ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public  View getView(int position,  View convertView,  ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent){
        final View view = mInflater.inflate(mResource, parent, false);

        TextView retardMin = view.findViewById(R.id.retard);
        retardMin.setText(retard.get(position));

        return view;
    }
}
