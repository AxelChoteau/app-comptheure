package com.afpa.comptheure.database;

import com.afpa.comptheure.constante.ConstanteDataBase;
import java.util.ArrayList;

/**
 * Classe gérant la table de session
 *
 * @author Najim
 */
public class SessionTable {

    public static final String SESSION_TABLE_NAME = "session";
    public static final String SESSION_COLUMN_NAME = "nom";
    public static final String SESSION_COLUMN_DATE = "date";
    public static final String SESSION_SELECT_All = ConstanteDataBase.SELECTALL
            + SESSION_TABLE_NAME + ConstanteDataBase.ORDER_BY
            + SESSION_COLUMN_NAME + ConstanteDataBase.VIRGULE
            + SESSION_COLUMN_DATE;

    /**
     * Constructeur sans paramètre
     */
    public SessionTable() {
    }

    public ArrayList<String> getList() {
        ArrayList<String> list = new ArrayList<>();
        list.add("2018");
        list.add("2019");
        return list;
    }
}