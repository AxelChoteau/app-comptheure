package com.afpa.comptheure.database;

import android.content.Context;
import java.util.ArrayList;

public class StagiaireRetardTable {

    public static final String TEMPS_COLUMN_DATE = "date";
    public static final String TEMPS_COLUMN_JOURNEE = "journee";
    public static final String TEMPS_COLUMN_CODE = "code";
    public static final String TEMPS_COLUMN_RETARD = "retard";

    private MySQLiteHelper maBaseSQLite; // notre gestionnaire du fichier SQLite
    private Context context;

    public StagiaireRetardTable(Context context) {
        maBaseSQLite = new MySQLiteHelper(context);
        this.context = context;
        maBaseSQLite = new MySQLiteHelper(context);
    }

    public ArrayList<String> getList() {
        ArrayList<String> list = new ArrayList<>();
        list.add("082018;1;10");
        list.add("022018;0;10");
        return list;
    }
}