package com.afpa.comptheure.objet;

import lombok.Data;

@Data
public class StagiaireCompetence {

    private String name;
    private String etat;

    public StagiaireCompetence(String name, String etat){
        this.name=name;
        this.etat=etat;
    }
}