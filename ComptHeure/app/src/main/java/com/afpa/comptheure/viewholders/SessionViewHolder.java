package com.afpa.comptheure.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.afpa.comptheure.R;

import lombok.Getter;

@Getter
public class SessionViewHolder extends RecyclerView.ViewHolder {
    TextView sessionName;
    public SessionViewHolder(View v){
        super(v);
        sessionName = itemView.findViewById(R.id.main_holder_session);
    }
}
