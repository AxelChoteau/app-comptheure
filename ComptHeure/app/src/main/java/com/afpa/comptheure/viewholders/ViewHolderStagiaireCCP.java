package com.afpa.comptheure.viewholders;

import com.afpa.comptheure.adapter.AdapterStagiaireCompetence;
import com.afpa.comptheure.adapter.AdapterStagiaireCCP;
import android.support.v7.widget.LinearLayoutManager;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.afpa.comptheure.R;
import com.afpa.comptheure.adapter.AdapterStagiaireCCP;
import com.afpa.comptheure.adapter.AdapterStagiaireCompetence;
import com.afpa.comptheure.objet.Stagiaire;
import com.afpa.comptheure.objet.StagiaireCCP;
import com.afpa.comptheure.objet.StagiaireCompetence;

import java.util.ArrayList;
import android.view.View;
import lombok.Getter;

public class ViewHolderStagiaireCCP extends RecyclerView.ViewHolder{

    private AdapterStagiaireCCP adapter;
    private AppCompatActivity activity;

    @Getter
    private ConstraintLayout subItem;

    private TextView txtCCPName;
    private RecyclerView competenceList;
    @Getter
    private ImageView imgExpand;

    public ViewHolderStagiaireCCP(View v, AdapterStagiaireCCP adapter, AppCompatActivity activity){
        super(v);
        this.adapter=adapter;
        this.activity=activity;
        txtCCPName = itemView.findViewById(R.id.stagiaire_ccp_name);
        subItem=itemView.findViewById(R.id.stagiaire_ccp_subitem);
        competenceList=itemView.findViewById(R.id.stagiaire_competence_list);
        imgExpand=itemView.findViewById(R.id.competence_stagiaire_expand);
    }

    public void bind(StagiaireCCP ccp){
        Log.i("Axel", "bind: ccpName="+ccp.getName());
        txtCCPName.setText(ccp.getName());

        competenceList.setHasFixedSize(true);
        competenceList.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
        AdapterStagiaireCompetence sAdapter = new AdapterStagiaireCompetence(activity, ccp.getCompetences());
        competenceList.setAdapter(sAdapter);
    }
}