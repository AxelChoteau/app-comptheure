package com.afpa.comptheure.objet;

import java.util.ArrayList;

public class CDI {

    public static Boolean ajout=false;
    private String formation = "cdi";
    private String session = "102018";
    private static ArrayList <String> listCompetence = new ArrayList<>();
    private static ArrayList <String> listStagiaire = new ArrayList<>();

    public CDI (){

        // Ajout de la formation dans la base de données
        Formation.insertFormation(formation);

        // Création de la session et ajout de la session
        Sessions.createSession();
        Sessions.insertSession(formation, session);

        // Création de la table compétence et ajout dans celle ci
        Competences.createCompetence(formation);
        for (String a : listCompetence){
            String [] tab = a.split(";");
            Competences.insertCompetence(formation, tab[0].replace("'", " "), tab[1].replace("'", " "));
        }

        // Création de la table stagiaire et ajout dans celle ci
        Stagiaire.createStagiaire(formation,session );
        for (String b : listStagiaire){
            String [] tab = b.split(";");
            Stagiaire.insertStagiaire(formation + "_" + session+"_"+"stagiaire", tab[0], tab[1], tab[2]);
            String email = tab[2].replace("@", "arobase").replace(".", "point").trim();
            Stagiaire.createLate(email);
            Stagiaire.createSkill(email);
            Stagiaire.createListCompetences(Competences.selectAll(formation), email);
        }
    }

    static {
        listCompetence.add("Développer des composants d'interface;maquetter une application");
        listCompetence.add("Développer des composants d'interface;Développer une interface utilisateur");
        listCompetence.add("Développer des composants d'interface;Développer des composant d'accès aux données");
        listCompetence.add("Développer des composants d'interface;Développer des page web en lien avec une de base de données");
        listCompetence.add("Développer la persistance des données;Concevoir une base de données");
        listCompetence.add("Développer la persistance des données;Mettre en place une base de données");
        listCompetence.add("Développer la persistance des données;Développer des composants dans le langage d'une base de données");
        listCompetence.add("Développer la persistance des données;Utiliser l'anglais dans son activité professionnelle en informatique");
        listCompetence.add("Développer une application n-tiers;Concevoir une application");
        listCompetence.add("Développer une application n-tiers;Collaborer à la gestion d'un projet informatique");
        listCompetence.add("Développer une application n-tiers;Développer des composants metier");
        listCompetence.add("Développer une application n-tiers;Construire une application de mobilité numerique");
        listCompetence.add("Développer une application n-tiers;Préparer et exécuter les plans de tests d'une application");
        listCompetence.add("Développer une application n-tiers;Préparer et exécuter le déploiement d'une application");

        listStagiaire.add("Snauwaert;Brandon;brandon.snauwaert@afpa.com");
        listStagiaire.add("Javourez;Thomas;thomas.javourez@afpa.com");
        listStagiaire.add("Choteau;Axel;axel.choteau@afpa.com");
        listStagiaire.add("Sailly;Damien;damien.sailly@afpa.com");
        listStagiaire.add("Erradja;Amine;amine.erradja@afpa.com");
        listStagiaire.add("Gozdzicki;Thierry;thierry.gozdzicki@afpa.com");
        listStagiaire.add("Graindorge;Maria;maria.graindorge@afpa.com");
        listStagiaire.add("Olivier;Cassandra;cassandra.olivier@afpa.com");
        listStagiaire.add("Jaadar;Najim;njaadar@gmail.com");
        listStagiaire.add("Machta;Ossama;ossama.machta@afpa.com");
        listStagiaire.add("Gambier;Roman;roman.gambier@afpa.com");
        listStagiaire.add("Buasa;Sergey;sergey.buasa@afpa.com");
    }
}