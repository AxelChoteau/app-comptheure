package com.afpa.comptheure;

import com.afpa.comptheure.adapter.AdapterStagiaireCCP;
import android.support.v7.widget.LinearLayoutManager;

import com.afpa.comptheure.objet.Stagiaire;
import com.afpa.comptheure.objet.StagiaireCompetence;
import com.afpa.comptheure.tools.Graph;
import com.github.mikephil.charting.charts.PieChart;
import com.afpa.comptheure.objet.StagiaireCCP;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;
import android.widget.Button;
import java.util.ArrayList;
import android.os.Bundle;
import android.view.View;
import java.util.List;

public class ActivityStagiaireCompetence extends ActivityAbstract {

    private TextView txtStagiaireName;
    private RecyclerView ccpList;
    private PieChart ccpPieChart;
    private Button btnApply;
    private AdapterStagiaireCCP adapter;
    private String[] stagiaire;
    private String mail;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stagiaire_competence_activity);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        stagiaire = getIntent().getStringExtra("stagiaire").split(";");
        mail=stagiaire[2];
        txtStagiaireName=findViewById(R.id.stagiaire_stagiaire_name);
        txtStagiaireName.setText(stagiaire[0]+" "+stagiaire[1]);

        //données brut pour test
        StagiaireCCP ccp1 = new StagiaireCCP("ccp1");
        StagiaireCompetence comp1 = new StagiaireCompetence("comp1", "a");
        StagiaireCompetence comp2 = new StagiaireCompetence("comp2", "e");
        ccp1.getCompetences().add(comp1);
        ccp1.getCompetences().add(comp2);
        List<StagiaireCCP> listCCP = new ArrayList<>();
        listCCP.add(ccp1);
        //fin données brut
//        Stagiaire.createObject(Stagiaire.selectSkill(mail));
        ccpList=findViewById(R.id.stagiaire_ccp_list);
        ccpList.setHasFixedSize(true);
        ccpList.setLayoutManager(new LinearLayoutManager(this));
        adapter = new AdapterStagiaireCCP(this,Stagiaire.createObject(Stagiaire.selectSkill(mail.replace(".", "point").replace("@","arobase").trim())));
        ccpList.setAdapter(adapter);
        ccpPieChart=findViewById(R.id.competence_stagiaire_piechart);
        Graph.drawGraph(this, ccpPieChart, Stagiaire.selectSkill(mail.replace(".", "point").replace("@","arobase").trim()));
        btnApply=findViewById(R.id.stagiaire_competence_apply);
        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Stagiaire.updateCompetences(adapter.getCcpStagiaire(),mail.replace(".", "point").replace("@","arobase").trim());
               Graph.drawGraph(ActivityStagiaireCompetence.this, ccpPieChart, Stagiaire.selectSkill(mail.replace(".", "point").replace("@","arobase").trim()));
            }
        });
    }
}