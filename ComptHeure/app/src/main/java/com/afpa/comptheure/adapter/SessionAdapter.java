package com.afpa.comptheure.adapter;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afpa.comptheure.MainActivity;
import com.afpa.comptheure.R;
import com.afpa.comptheure.listener.SessionListener;
import com.afpa.comptheure.viewholders.SessionViewHolder;
import java.util.ArrayList;

public class SessionAdapter extends RecyclerView.Adapter<SessionViewHolder>{

    private ArrayList<String> sessions;
    private MainActivity activity;

    public SessionAdapter(ArrayList<String> sessions, MainActivity activity){
        this.sessions=sessions;
        this.activity=activity;
    }

    @NonNull
    @Override
    public SessionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.main_session_holder, parent, false);
        return new SessionViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull SessionViewHolder viewHolder, int position) {
        viewHolder.getSessionName().setText(sessions.get(position));
        viewHolder.getSessionName().setOnClickListener(new SessionListener(activity, sessions.get(position)));
    }

    @Override
    public int getItemCount() {
        return sessions.size();
    }
}