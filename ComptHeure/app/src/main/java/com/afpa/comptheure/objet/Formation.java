package com.afpa.comptheure.objet;

import static com.afpa.comptheure.constante.ConstanteDataBase.PARENTHESE_OUVR_QUOTE;
import static com.afpa.comptheure.constante.ConstanteDataBase.QUOTE_PARENTHESE_FERM;
import static com.afpa.comptheure.database.FormationTable.FORMATION_COLUMN_NAME;
import static com.afpa.comptheure.database.FormationTable.FORMATION_TABLE_NAME;
import static com.afpa.comptheure.constante.ConstanteDataBase.INSERT_INTO;
import static com.afpa.comptheure.constante.ConstanteDataBase.SELECTALL;
import static com.afpa.comptheure.constante.ConstanteDataBase.VALUES;
import static com.afpa.comptheure.constante.ConstanteDataBase.WHERE;
import static com.afpa.comptheure.constante.ConstanteDataBase.QUOTE;
import static com.afpa.comptheure.constante.ConstanteDataBase.EGALE;
import com.afpa.comptheure.database.DatabaseAndroid;
import lombok.Data;

/**
 * Classe permettant la gestion des formations
 *
 * @author Thierry
 */
@Data
public class Formation {

    private String name;

    /**
     * Constructeur avec un paramètre
     *
     * @param name est le nom de la formation
     */
    public Formation (String name){
        this.name = name;
    }

    /**
     * Méthode pour l'ajout de la formation dans la base de données
     *
     * @param name contient le nom de la formation a ajouter
     */
    public static boolean insertFormation(String name){

        // Condition d'ajout dans la base de données si données non existante
        if (!DatabaseAndroid.onExist(SELECTALL + FORMATION_TABLE_NAME
                + WHERE + FORMATION_COLUMN_NAME + EGALE + QUOTE + name + QUOTE)){

            // Méthode insert into vers classe abstract
            DatabaseAndroid.onInsert(INSERT_INTO + FORMATION_TABLE_NAME
                    + VALUES + PARENTHESE_OUVR_QUOTE + name + QUOTE_PARENTHESE_FERM);
            return true;
        } return false;
    }
}