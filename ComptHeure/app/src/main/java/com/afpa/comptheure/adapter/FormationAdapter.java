package com.afpa.comptheure.adapter;

import com.afpa.comptheure.viewholders.FormationViewHolder;
import com.afpa.comptheure.objet.SessionActive;
import android.support.v7.widget.RecyclerView;
import android.support.annotation.NonNull;
import com.afpa.comptheure.MainActivity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.afpa.comptheure.R;
import android.view.View;
import java.util.List;

public class FormationAdapter extends RecyclerView.Adapter<FormationViewHolder>{

    private List<String> formations;
    private int mExpandedPosition= -1;
    private RecyclerView recyclerView = null;
    private MainActivity activity;

    public FormationAdapter(List<String> formations, MainActivity activity){
        this.activity=activity;
        this.formations=formations;
    }

    @NonNull
    @Override
    public FormationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.main_recyclerview_holder, parent, false);
        return new FormationViewHolder(v, this, activity);
    }

    @Override
    public void onBindViewHolder(@NonNull  FormationViewHolder viewHolder, final int position) {
        if (formations.size()==0){
            initEmptyAdapter(viewHolder);
            return;
        }

        final String formation = formations.get(position);
        SessionActive.formation_name = formations.get(position).toUpperCase().trim();
        viewHolder.bind(formation);
        viewHolder.getSubItem().setVisibility(View.GONE);

        final boolean isExpanded = position==mExpandedPosition;
        final int pos=position;

        viewHolder.getSubItem().setVisibility(isExpanded?View.VISIBLE:View.GONE);
        viewHolder.getImgExpand().setImageResource(isExpanded? R.drawable.icons8_collapse_arrow_36:R.drawable.icons8_expand_arrow_36);
        viewHolder.itemView.setActivated(isExpanded);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mExpandedPosition = isExpanded ? -1:pos;
//                TransitionManager.beginDelayedTransition(recyclerView);
                notifyDataSetChanged();
                activity.setFormationName(formations.get(position));
                SessionActive.formation_choix = formations.get(position).trim();
            }
        });
    }

    @Override
    public int getItemCount() {
        return formations.size()==0?1:formations.size();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

        this.recyclerView = recyclerView;
    }

    public void initEmptyAdapter(FormationViewHolder viewHolder){
        viewHolder.getImgExpand().setVisibility(View.GONE);
        viewHolder.getSubItem().setVisibility(View.GONE);
        viewHolder.itemView.findViewById(R.id.divider).setVisibility(View.GONE);
        viewHolder.getFormationName().setText(activity.getString(R.string.no_formation));
    }

    public void setFormations(String formationName){
        formations.add(formationName);
    }
}