package com.afpa.comptheure.listener;

import android.app.DatePickerDialog;
import android.content.Context;
import android.text.InputType;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.afpa.comptheure.GestionActivity;
import com.afpa.comptheure.RetardActivity;
import com.afpa.comptheure.objet.Stagiaire;

import java.util.Calendar;

public class DateClickListener implements View.OnClickListener {

    private Context context;
    private TextView txtDate;
    private int mYear, mMonth, mDay;
    public DateClickListener(Context context, TextView txtDate){
        this.context=context;
        this.txtDate=txtDate;
    }
    @Override
    public void onClick(View v) {
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            String date= (dayOfMonth<10?"0":"")+dayOfMonth+"/"+((monthOfYear + 1)<10?"0":"")+(monthOfYear + 1) + "/" + year;
                            txtDate.setText(date);
                            GestionActivity gestionActivity = (GestionActivity) context;
                            gestionActivity.getGestionPagerAdapter().getRetardActivity().getAdapter().setRetard(Stagiaire.createListLate(gestionActivity.getListStagiaires(), gestionActivity.getTxtDate().getText().toString().replace("/","").trim(), gestionActivity.getASwitch().getText().toString()));
                            gestionActivity.getGestionPagerAdapter().getRetardActivity().getAdapter().notifyDataSetChanged();
                        }
                    }, mYear, mMonth, mDay);

            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
//            GestionActivity gestionActivity = (GestionActivity) context;
//        gestionActivity.getGestionPagerAdapter().getRetardActivity().getAdapter().setRetard(Stagiaire.createListLate(gestionActivity.getListStagiaires(), gestionActivity.getTxtDate().getText().toString().replace("/","").trim(), gestionActivity.getASwitch().getText().toString()));
//        gestionActivity.getGestionPagerAdapter().getRetardActivity().getAdapter().notifyDataSetChanged();
        }
    }

