package com.afpa.comptheure;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import com.afpa.comptheure.adapter.FormationAdapter;
import com.afpa.comptheure.database.CompetenceTable;
import com.afpa.comptheure.database.DatabaseAndroid;
import com.afpa.comptheure.database.FormationTable;
import com.afpa.comptheure.objet.CDI;
import com.afpa.comptheure.objet.Sessions;
import com.afpa.comptheure.popup.PopupFormation;

import lombok.Getter;
import lombok.Setter;

public class MainActivity extends ActivityAbstract {

    private FormationTable ft;

    public static Context context =null;

    private FormationAdapter adapter;

    @Getter
    @Setter
    private String formationName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        context=getApplicationContext();
        ft=new FormationTable(context);
        if (CDI.ajout==true){
            new CDI();
        }
        Sessions.createSession();
        RecyclerView mainRV = findViewById(R.id.mainactivity_list_formation);
        mainRV.setHasFixedSize(true);
        mainRV.setLayoutManager(new LinearLayoutManager(this));
        adapter = new FormationAdapter(DatabaseAndroid.onSelect(FormationTable.FORMATION_SELECT_ALL),this);
        mainRV.setAdapter(adapter);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new PopupFormation(MainActivity.this, getString(R.string.formation_title), R.layout.main_popup_newformation, adapter).initPopup();
            }
        });
    }



    public boolean insertFormation(String formation){
        return true;
//        return ft.insertFormation(formation);
    }
}