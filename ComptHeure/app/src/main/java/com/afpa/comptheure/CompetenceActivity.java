package com.afpa.comptheure;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import com.afpa.comptheure.objet.Competences;
import com.afpa.comptheure.popup.PopupCompetence;
import android.support.v7.widget.RecyclerView;
import com.afpa.comptheure.adapter.CCPAdapter;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import android.view.View;
import android.os.Bundle;

import lombok.Getter;

public class CompetenceActivity extends ActivityAbstract {

    private TextView txtFormationName;
    private RecyclerView listCCP;
    @Getter
    private String formationName;
    @Getter
    private CCPAdapter adapter;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.competence_main_activity);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        formationName = getIntent().getStringExtra("formation").trim();
        txtFormationName = findViewById(R.id.competence_formation);
        txtFormationName.setText(formationName);

        listCCP= findViewById(R.id.competence_ccp);
        listCCP.setHasFixedSize(true);
        listCCP.setLayoutManager(new LinearLayoutManager(this));
//        String[] ccp = {"test"};
//        List<String> ccp = new ArrayList<>();
//        ccp.add("test");
//        ccp.add("test");
        adapter = new CCPAdapter (this, Competences.createListCcp(Competences.selectCcp(formationName)));
        listCCP.setAdapter(adapter);

        FloatingActionButton newCompetence = findViewById(R.id.fab);
        newCompetence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new PopupCompetence(CompetenceActivity.this, getString(R.string.competence_title), R.layout.competence_new_competence, adapter, formationName).initPopup();
            }
        });
    }
}