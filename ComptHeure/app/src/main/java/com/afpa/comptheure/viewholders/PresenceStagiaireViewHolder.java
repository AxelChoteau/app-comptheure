package com.afpa.comptheure.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.afpa.comptheure.R;
import com.afpa.comptheure.adapter.PresenceStagiaireAdapter;

import lombok.Getter;

@Getter
public class PresenceStagiaireViewHolder extends RecyclerView.ViewHolder{

    private TextView stagiaireName;
    private CheckBox selectStagiaire;

    public PresenceStagiaireViewHolder(View v, final PresenceStagiaireAdapter adapter){
        super(v);
        stagiaireName=itemView.findViewById(R.id.presence_stagiaire_name);
        selectStagiaire=itemView.findViewById(R.id.presence_select_stagiaire);
        selectStagiaire.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                adapter.getStagiaires().get(getAdapterPosition()).setSelected(isChecked);
            }
        });
    }
}
