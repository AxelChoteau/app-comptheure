package com.afpa.comptheure.adapter;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afpa.comptheure.R;
import com.afpa.comptheure.objet.Stagiaire;
import com.afpa.comptheure.objet.StagiaireCompetence;
import com.afpa.comptheure.viewholders.ViewHolderStagiaireCompetence;

import java.util.List;

import lombok.Getter;

public class AdapterStagiaireCompetence extends RecyclerView.Adapter<ViewHolderStagiaireCompetence>{

    private AppCompatActivity activity;

    @Getter
    private List<StagiaireCompetence> competences;

    public AdapterStagiaireCompetence(AppCompatActivity activity, List<StagiaireCompetence> competences){
        this.activity=activity;
        this.competences=competences;
    }

    @NonNull
    @Override
    public ViewHolderStagiaireCompetence onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.stagiaire_competence_holder, parent, false);
        return new ViewHolderStagiaireCompetence(v, this, activity);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderStagiaireCompetence viewHolder, int position) {
        viewHolder.getTxtCompetenceName().setText(competences.get(position).getName());
        switch (competences.get(position).getEtat()){
            case "a":
                viewHolder.getBtnAcquis().setChecked(true);
                break;
            case "e":
                viewHolder.getBtnEnCours().setChecked(true);
                break;
            case "n":
                viewHolder.getBtnNonAcquis().setChecked(true);
        }

    }

    @Override
    public int getItemCount() {
        return competences.size();
    }
}
