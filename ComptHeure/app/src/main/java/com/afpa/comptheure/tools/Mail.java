package com.afpa.comptheure.tools;


import com.afpa.comptheure.objet.Stagiaire;

import java.util.ArrayList;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Classe pour l'envoi des emails
 *
 * @author Najim
 */
public class Mail {


    // liste des attributs
    private static final String PROTOCOLE = "smtp";
    private static final String SERVEUR = "smtp.gmail.com";
    private static final String DEPUIS = "afpa.cdi.18290@gmail.com";
    private static final String USERNAME = "afpa.cdi.18290@gmail.com";
    private static final String PASSWORD = "AFPA-CDI-18290";
    private static final String OBJET = "AFPA - Demande de justificatifs";
    private static final String MESSAGE = "Bonjour, \r\n\r\n"
            + "Nous avons remarqué votre absence, celle ci n'étant pas prévue, "
            + "nous vous remercions de bien vouloirs nous transmettre un justificatif dans les 48h.\r\n"
            + "Cordialement\r\n\r\n"
            + "AFPA ROUBAIX ";



    /**
     * Méthode pour l'envoi d'emails groupés
     *
     * @param to contient la liste des stagiaires
     */
    public static void envoiMail (Stagiaire to) throws AddressException, MessagingException {

        // Création de la session
        Properties props = new Properties();
        props.put("mail.smtp.host", SERVEUR);
        props.put("mail.smtp.auth", "true");
        props.put ("mail.smtp.starttls.enable", "true");
        Session session = Session.getDefaultInstance(props);
        session.setDebug(true);

        // Création du message
        MimeMessage mimeMessage = new MimeMessage(session);
        mimeMessage.setFrom(new InternetAddress(DEPUIS));
        mimeMessage.setSubject(OBJET);
        mimeMessage.setText(MESSAGE);

        // ajout des destinataires

        mimeMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(to.getEmail()));

        // envoi du message
        Transport tr = session.getTransport(PROTOCOLE);
        tr.connect(SERVEUR, USERNAME, PASSWORD);
        mimeMessage.saveChanges();
        tr.sendMessage(mimeMessage,mimeMessage.getAllRecipients());
        tr.close();
    }
}