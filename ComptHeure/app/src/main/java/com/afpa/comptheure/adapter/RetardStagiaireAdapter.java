package com.afpa.comptheure.adapter;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.afpa.comptheure.R;
import com.afpa.comptheure.RetardActivity;
import com.afpa.comptheure.viewholders.PresenceStagiaireViewHolder;
import com.afpa.comptheure.viewholders.RetardStagiaireViewHolder;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class RetardStagiaireAdapter extends RecyclerView.Adapter<RetardStagiaireViewHolder>{

    private AppCompatActivity activity;

    private RecyclerView recyclerView;
@Getter
@Setter
    private List<String> retard;

    public RetardStagiaireAdapter(AppCompatActivity activity, List<String> retard){
        this.activity=activity;
        this.retard=retard;
    }

    @NonNull
    @Override
    public RetardStagiaireViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.retard_stagiaire_list_holder, parent, false);
        return new RetardStagiaireViewHolder(v, this);
    }

    @Override
    public void onBindViewHolder(@NonNull RetardStagiaireViewHolder viewHolder, int position) {
        String[] data = retard.get(position).split(";");
        viewHolder.getStagiaireName().setText(data[0].trim());
        viewHolder.getStagiaireRetard().setText(data[1].trim()+ " min");
    }

    @Override
    public int getItemCount() {
        return retard.size();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

        this.recyclerView = recyclerView;
    }



    public void addRetard(String retard){
        this.retard.add(retard);
    }
}
