package com.afpa.comptheure.adapter;

import android.support.annotation.NonNull;
import android.support.transition.TransitionManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afpa.comptheure.CompetenceActivity;
import com.afpa.comptheure.MainActivity;
import com.afpa.comptheure.R;
import com.afpa.comptheure.objet.SessionActive;
import com.afpa.comptheure.viewholders.CCPViewHolder;
import com.afpa.comptheure.viewholders.FormationViewHolder;

import java.util.List;

import lombok.Setter;


public class CCPAdapter extends RecyclerView.Adapter<CCPViewHolder>{
@Setter
    private List<String> ccp;
    private int mExpandedPosition= -1;
    private RecyclerView recyclerView = null;
    private CompetenceActivity activity;

    public CCPAdapter(CompetenceActivity activity, List<String> ccp){
        this.activity=activity;
        this.ccp=ccp;
    }

    @NonNull
    @Override
    public CCPViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.competence_ccp_holder, parent, false);
        return new CCPViewHolder(v, this, activity);
    }

    @Override
    public void onBindViewHolder(@NonNull CCPViewHolder viewHolder, int position) {
        final String ccpName = ccp.get(position);
//        SessionActive.formation_name = formations.get(position).toUpperCase().trim();
        viewHolder.bind(ccpName);
        viewHolder.getSubItem().setVisibility(View.GONE);

        final boolean isExpanded = position==mExpandedPosition;
        final int pos=position;

        viewHolder.getSubItem().setVisibility(isExpanded?View.VISIBLE:View.GONE);
        viewHolder.getImgExpand().setImageResource(isExpanded? R.drawable.icons8_collapse_arrow_36:R.drawable.icons8_expand_arrow_36);
        viewHolder.itemView.setActivated(isExpanded);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mExpandedPosition = isExpanded ? -1:pos;
//                TransitionManager.beginDelayedTransition(recyclerView);
                notifyDataSetChanged();
                SessionActive.formation_choix = ccp.get(pos).trim();
            }
        });
    }

    @Override
    public int getItemCount() {
        return ccp.size();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

        this.recyclerView = recyclerView;
    }
}
