package com.afpa.comptheure;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;

import com.afpa.comptheure.listener.DateClickListener;
import com.afpa.comptheure.objet.Stagiaire;
import com.afpa.comptheure.tools.Retard;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
public class GestionActivity extends ActivityAbstract {

@Getter
    private List<Stagiaire> listStagiaires;
    private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    private ImageButton btnDate;
    private TextView txtDate;
    private Switch aSwitch;
    @Getter
    @Setter
    private List<String> retards;

    GestionPagerAdapter gestionPagerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String session = getIntent().getStringExtra("gestion");
        setContentView(R.layout.gestion_activity);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        txtDate=findViewById(R.id.gestion_txt_date);
        txtDate.setText(sdf.format(new Date()));

        btnDate=findViewById(R.id.gestion_btn_select_date);
        btnDate.setOnClickListener(new DateClickListener(this, txtDate));

        aSwitch=findViewById(R.id.gestion_selector);
        aSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (aSwitch.isChecked()){
                    aSwitch.setText(GestionActivity.this.getString(R.string.PM));
                } else{
                    aSwitch.setText(GestionActivity.this.getString(R.string.AM));
                }
                retards=Stagiaire.createListLate(getListStagiaires(), getTxtDate().getText().toString().replace("/","").trim(), aSwitch.getText().toString());

                getGestionPagerAdapter().getRetardActivity().getAdapter().setRetard(Stagiaire.createListLate(getListStagiaires(), getTxtDate().getText().toString().replace("/","").trim(), getASwitch().getText().toString()));
                getGestionPagerAdapter().getRetardActivity().getAdapter().notifyDataSetChanged();
            }
        });
        listStagiaires=Stagiaire.createListStagiaires(Stagiaire.selectStagiaire(session));

        retards=Stagiaire.createListLate(getListStagiaires(), getTxtDate().getText().toString().replace("/","").trim(), aSwitch.getText().toString());




        // Get the ViewPager and set it's PagerAdapter so that it can display items
        ViewPager viewPager = findViewById(R.id.viewpager);
        gestionPagerAdapter = new GestionPagerAdapter(getSupportFragmentManager(),
                this);
        viewPager.setAdapter(gestionPagerAdapter);

        // Give the TabLayout the ViewPager
        TabLayout tabLayout = findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);
    }
}
